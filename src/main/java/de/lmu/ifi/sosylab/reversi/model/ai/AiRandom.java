package de.lmu.ifi.sosylab.reversi.model.ai;

import de.lmu.ifi.sosylab.reversi.model.Cell;
import de.lmu.ifi.sosylab.reversi.model.Model;
import de.lmu.ifi.sosylab.reversi.model.Player;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * The very simplest AI for the reversi game.
 *
 * <p>It will just execute a random move without any consideration of the value of the move.
 */
public class AiRandom implements AiEngine {

  private Model model;
  private Random random;

  /**
   * Initializes the random AI engine.
   *
   * @param model used for the game
   */
  public AiRandom(Model model) {
    this.model = model;
    random = new Random();
  }

  /**
   * Computes the concrete move of the AI.
   *
   * @return the cell the ai wants to move to
   */
  Cell computeMove() {

    List<Cell> moves =
        model.getPossibleMovesForDisk(model.getState().getCurrentPlayer()).stream()
            .collect(Collectors.toList());
    int size = moves.size();
    if (size == 0) {
      throw new IllegalArgumentException("game should be over");
    }

    int randomIndex = random.nextInt(size);
    Cell move = moves.get(randomIndex);
    return move;
  }

  @Override
  public Cell getTarget() {
    return computeMove();
  }

  @Override
  public void setComputer(Player computer) {
    throw new UnsupportedOperationException("This implementation does not need an Computer");
  }

  @Override
  public void setHeuristic(Heuristic heuristic) {
    throw new UnsupportedOperationException("This implementation does not need an heuristic");
  }
}
