package de.lmu.ifi.sosylab.reversi.model.communication;

import de.lmu.ifi.sosylab.reversi.model.GameState;
import de.lmu.ifi.sosylab.reversi.model.Player;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class ReversiClient implements Client {

  public boolean secondPlayer = false;
  public boolean connected = false;

  private static final int MAX_CONNECTION_ATTEMPTS = 50;
  private TcpConnection connection = null;
  private Player player;
  private int gameId;
  private String mode;

  /**
   * Generates a client for the network game.
   *
   * @param player player of the client
   * @param id the future id of the network game
   * @param mode <code>"match"</code> or <code>"random"</code>
   */
  public ReversiClient(Player player, int id, String mode) {
    this.gameId = id;
    this.player = player;
    this.mode = mode;
    System.out.println("Client started");
  }

  @Override
  public TcpConnection connectToServer(
      String remoteServerAddress,
      int serverPort,
      int localPort,
      int sendBufferSize,
      int receiveBufferSize)
      throws Exception {

    TcpConnection connection = null;

    InetAddress localAddress = null;

    int attempts = 0;
    while ((!connected)) {
      try {
        connection =
            new TcpConnection(
                new Socket(remoteServerAddress, serverPort, localAddress, localPort),
                sendBufferSize,
                receiveBufferSize);
        connected = true;
      } catch (IOException e) {
        attempts++;
        try {
          Thread.sleep(100);
        } catch (Exception e2) {
          throw new Exception(e2);
        }
      }
      if (attempts >= MAX_CONNECTION_ATTEMPTS) {
        throw new IOException();
      }
    }
    return connection;
  }

  /**
   * Get the game that are currently open on the communication.
   *
   * @return A list of the waiting games
   * @throws Exception error
   */
  public Pdu[] getOpenGames() throws Exception {
    Pdu startMessage = createMessage(this.gameId, null, getPlayer(), this.mode, "games");
    try {
      connection.send(startMessage.toJson());
    } catch (Exception e) {
      System.out.println("Exception during send");
      throw new Exception();
    }
    Pdu newPdu = receive();
    return newPdu.getWaitingGames();
  }

  /**
   * Connect to the communication and start a new Game.
   *
   * @throws Exception error, if something goes wrong while connecting to communication
   */
  public void connect(InetAddress address) throws Exception {
    connection = connectToServer(address.getHostAddress(), 50000, 0, 400000, 400000);
    System.out.println("Successfully connected!");
  }

  @Override
  public void syncState(GameState state, String type) throws Exception {
    Pdu startMessage = createMessage(this.gameId, state, getPlayer(), this.mode, type);
    try {
      connection.send(startMessage.toJson());
    } catch (Exception e) {
      System.out.println("Exception during send");
      throw new Exception();
    }
  }

  @Override
  public Pdu checkMove(GameState state) throws Exception {
    Pdu updateMessage = createUpdateMessage(this.gameId, state, this.player, this.mode);
    try {
      connection.send(updateMessage.toJson());
      return receive();
    } catch (Exception e) {
      System.out.println("Exception during send or receive");
      throw new Exception();
    }
  }

  @Override
  public void resetGame(GameState state) throws Exception {
    Pdu resetMessage = createMessage(this.gameId, state, this.player, "reset", "reset");
    try {
      connection.send(resetMessage.toJson());
    } catch (Exception e) {
      System.out.println("Exception during send of reset");
      throw new Exception();
    }
  }

  @Override
  public void removeGame() throws Exception {
    Pdu deleteMessage = createMessage(this.gameId, null, this.player, this.mode, "deleteGame");
    try {
      connection.send(deleteMessage.toJson());
    } catch (Exception e) {
      System.out.println("Exception during send of delete");
      throw new Exception();
    }
  }

  @Override
  public Pdu receive() {
    try {
      String receivedJson = (String) connection.receive();
      Pdu receivedPdu = Pdu.readPdu(receivedJson);
      secondPlayer = true;
      return receivedPdu;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public void close() throws Exception {
    try {
      connection.close();
      connected = false;
      System.out.println("Connection closed");
    } catch (Exception e) {
      System.out.println("Exception during close");
      throw new Exception();
    }
  }

  /**
   * Create start-Pdu for Matchmaking.
   *
   * @return Pdu
   */
  private static Pdu createMessage(
      int gameId, GameState state, Player player, String mode, String type) {
    return new Pdu(gameId, type, state, player, mode);
  }

  /**
   * Create update-Pdu.
   *
   * @return Pdu
   */
  private static Pdu createUpdateMessage(int gameId, GameState state, Player player, String mode) {
    return new Pdu(gameId, "update", state, player, mode);
  }

  public Player getPlayer() {
    return player;
  }
}
