package de.lmu.ifi.sosylab.reversi.model.communication;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * Implementation of the Tcp Connection.
 *
 * @author mandlp
 */
public class TcpConnection {

  private DataOutputStream out;
  private DataInputStream in;

  private Socket socket;

  /**
   * Creates a TcpConnection.
   *
   * @param socket the socket
   * @param sendBufferSize bufferSize for sending
   * @param receiveBufferSize buffer size for receiving
   */
  public TcpConnection(Socket socket, int sendBufferSize, int receiveBufferSize) {
    this.socket = socket;

    try {

      out = new DataOutputStream(socket.getOutputStream());
      in = new DataInputStream(socket.getInputStream());

      socket.setReceiveBufferSize(receiveBufferSize);
      socket.setSendBufferSize(sendBufferSize);
      socket.setSoLinger(true, 1);

    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Method for receiving messages.
   *
   * @return the message
   * @throws IOException Error, if something goes wrong while reading the message.
   */
  public Serializable receive() throws IOException {
    if (!socket.isConnected()) {
      throw new EOFException();
    }
    socket.setSoTimeout(0);
    Reader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
    BufferedReader bufferedReader = new BufferedReader(reader);
    return bufferedReader.readLine();
  }

  /**
   * Method for sending messages.
   *
   * @param message the message
   * @throws IOException Error, if something goes wrong, while sending the message.
   */
  public void send(Serializable message) throws IOException {
    if (socket.isClosed()) {
      throw new IOException();
    }
    if (!socket.isConnected()) {
      throw new IOException();
    }
    try {
      Writer writer = new OutputStreamWriter(out, StandardCharsets.UTF_8);
      PrintWriter pw = new PrintWriter(writer);
      pw.println(message);
      pw.flush();
    } catch (Exception e) {
      throw new IOException();
    }
  }

  /**
   * Method for closing the connection.
   *
   * @throws IOException Error, if something goes wrong, while closing the connection.
   */
  public synchronized void close() throws IOException {
    try {
      out.flush();
      socket.close();
    } catch (Exception e) {
      throw new IOException(new IOException());
    }
  }
}
