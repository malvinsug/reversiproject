package de.lmu.ifi.sosylab.reversi.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Set;

/**
 * The main interface of the reversi model. It provides all necessary methods for accessing and
 * manipulating the data such that a game can be played successfully.
 *
 * <p>When something changes in the model, the model notifies its observers by firing a {@link
 * PropertyChangeEvent change-event}.
 */
public interface Model {

  static final String STATE_CHANGED = "State changed";
  static final String NEW_MOVE = "New move";
  static final String GAME_OVER = "Game Over";

  /**
   * Add a {@link PropertyChangeListener} to the model that will be notified about the changes made
   * to the reversi board.
   *
   * @param pcl the view that implements the listener.
   */
  void addPropertyChangeListener(PropertyChangeListener pcl);

  /**
   * Remove a listener from the model, which will then no longer be notified about any events in the
   * model.
   *
   * @param pcl the view that then no longer receives notifications from the model.
   */
  void removePropertyChangeListener(PropertyChangeListener pcl);

  /**
   * Sets a new game up. This includes putting all of the Disks on their initial positions, and
   * allowing {@link Player#BLACK} to make the first move.
   *
   * @throws IOException if any IOException occurs while starting the new game
   */
  void newGame() throws IOException;

  /**
   * Move Disk from one cell to another and deal with the consequences. Moving a Disk only works if
   * their is currently a game running, if the given cell contains a Disk of the current player, and
   * if moving to the other cell is a valid reversi move. After the move, it will be the turn of the
   * next player, unless he has to miss a turn because he can't move any Disk or the game is over.
   *
   * @param target cell to move Disk to
   * @return <code>true</code> if the move was successful, <code>false</code> otherwise
   */
  boolean move(Cell target);

  /**
   * Undo the last move if possible. In case of success, this will also inform the observers of the
   * model, so that they may take proper actions afterwards.
   *
   * <p>currently not implemented due to time issues
   */
  void undoMove();

  /**
   * Return the {@link GameState} specific to this class.
   *
   * @return The <code>GameState</code>-object.
   */
  GameState getState();

  /**
   * Set the {@link GameState} specific to this class.
   *
   * @param state The <code>GameState</code>-object.
   */
  void setState(GameState state);

  /**
   * Computes all possible moves for a selected cell.
   *
   * @param player is the players who's possible turns should be returned
   * @return A set of cells with all possible moves for the current selected Disk.
   */
  Set<Cell> getPossibleMovesForDisk(Player player);
}
