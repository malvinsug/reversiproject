package de.lmu.ifi.sosylab.reversi.model.communication;

import de.lmu.ifi.sosylab.reversi.model.GameState;
import de.lmu.ifi.sosylab.reversi.model.Phase;
import de.lmu.ifi.sosylab.reversi.model.Player;

public class GameThread extends Thread {

  private TcpConnection con1;
  private TcpConnection con2;
  private GameState state;
  private boolean connect;

  /**
   * Build a Thread that handles the connections of 2 players for one game.
   *
   * @param con1 connection of player 1
   * @param con2 connection of player 2
   * @param startMessage invitation message, to sync states
   */
  public GameThread(TcpConnection con1, TcpConnection con2, Pdu startMessage) {
    this.con1 = con1;
    this.con2 = con2;

    try {
      state = startMessage.getState();
      state.setCurrentPhase(Phase.RUNNING);
      startMessage.setState(state);

      // send start message to handle states of players
      sendToPlayer(con1, startMessage);
      sendToPlayer(con2, startMessage);
      sendToPlayer(con1, startMessage);
    } catch (Exception e) {
      handleClientError();
    }
    connect = true;

    // Thread bekommt einen Namen.
    this.setName("WorkerThread");
  }

  /** Method that implements the Tasks of the Thread. */
  public void run() {

    System.out.println(this.getName() + " gestartet");

    while (connect) {
      try {

        Pdu receivedPdu1 = receiveFromPlayer(con1); // receive  Message of first player
        String messageType1 = receivedPdu1.getMessageType();

        switch (messageType1) {
          case "start":
            sendToPlayer(con1, receivedPdu1);
            sendToPlayer(con2, receivedPdu1);
            break;
          case "join":
            System.out.println("There is no game to join");
            break;
          case "update":
            handleUpdatePlayer1(receivedPdu1);
            break;
          case "reset":
            handleGameReset(receivedPdu1, con1);
            break;
          default:
            handleClientError();
        }

        Pdu receivedPdu2 = receiveFromPlayer(con2); // receive message of second player
        String messageType2 = receivedPdu2.getMessageType();

        switch (messageType2) {
          case "start":
            System.out.println("The game is already started");
            break;
          case "join":
            sendToPlayer(con2, receivedPdu2);
            break;
          case "update":
            handleUpdatePlayer2(receivedPdu2);
            break;
          case "reset":
            handleGameReset(receivedPdu2, con2);
            break;
          default:
            handleClientError();
        }
      } catch (Exception e1) {
        handleClientError();
      }
    }
  }

  private void handleClientError() {
    try {
      System.out.println(
          this.getName()
              + ": Exception while talking to a client, Connection close,"
              + " Connection to the other player gets closed, too.");
      con1.close();
      con2.close();
      connect = false;
    } catch (Exception e2) {
      connect = false;
      e2.printStackTrace();
    }
  }

  private void handleGameEnd() {
    try {
      con1.close();
      con2.close();
      connect = false;
    } catch (Exception e) {
      connect = false;
      handleClientError();
      e.printStackTrace();
    }
  }

  private void handleGameReset(Pdu message, TcpConnection connection) {
    try {
      System.out.println("Game Reset.");
      if (connection.equals(con1)) {
        sendToPlayer(con2, message);
        sendToPlayer(con1, message);
      } else {
        sendToPlayer(con1, message);
      }
    } catch (Exception e) {
      handleClientError();
      e.printStackTrace();
    }
  }

  private void handleUpdatePlayer1(Pdu receivedPdu) throws Exception {
    GameState receivedState = receivedPdu.getState();
    if (receivedState.getCurrentPhase().equals(Phase.FINISHED)) {
      con2.send(receivedPdu);
      handleGameEnd();
    } else {
      // if state is valid update both
      if (isValidState(receivedState)) {
        sendToPlayer(con1, receivedPdu);
        sendToPlayer(con2, receivedPdu);
        this.state = receivedState;
      } else {
        // else reset state of sending player
        receivedPdu.setState(this.state);
        sendToPlayer(con1, receivedPdu);
      }
    }
  }

  private void handleUpdatePlayer2(Pdu receivedPdu) throws Exception {
    GameState receivedState = receivedPdu.getState();
    if (receivedState.getCurrentPhase().equals(Phase.FINISHED)) {
      con1.send(receivedPdu);
      handleGameEnd();
    } else {
      // if state is valid update both
      if (isValidState(receivedPdu.getState())) {
        sendToPlayer(con2, receivedPdu);
        sendToPlayer(con1, receivedPdu);
        this.state = receivedPdu.getState();
      } else {
        // else reset state of sending player
        receivedPdu.setState(this.state);
        sendToPlayer(con2, receivedPdu);
      }
    }
  }

  /**
   * Send a message to player.
   *
   * @param con The connection to the player
   * @param pdu the message
   * @throws Exception while sending to Player
   */
  private void sendToPlayer(TcpConnection con, Pdu pdu) throws Exception {
    try {
      // convert Pdu object into JSON string
      String toSend = pdu.toJson();
      con.send(toSend);
    } catch (Exception e) {
      System.out.println("Exception while sending to Player");
      handleClientError();
      throw new Exception();
    }
  }

  /**
   * Receive message from player.
   *
   * @param con The connection to the player
   * @throws Exception while receiving from player
   */
  private Pdu receiveFromPlayer(TcpConnection con) throws Exception {
    try {
      String receivedJson = (String) con.receive();
      Pdu receivedPdu = Pdu.readPdu(receivedJson);
      return receivedPdu;
    } catch (Exception e) {
      System.out.println("Exception while receiving from player");
      handleClientError();
      throw new Exception();
    }
  }

  /**
   * check if the received states is valid or not.
   *
   * @author malvin
   * @param state the received state from clients
   * @return if the received state is valid returns true.
   */
  private boolean isValidState(GameState state) {
    int currentSize =
        (this.state == null)
            ? 0
            : this.state.getAllCellsOfPlayer(Player.WHITE).size()
                + this.state.getAllCellsOfPlayer(Player.BLACK).size();
    int newSize =
        state.getAllCellsOfPlayer(Player.WHITE).size()
            + state.getAllCellsOfPlayer(Player.BLACK).size();

    if (newSize - currentSize != 1) {
      return false;
    }

    return true;
  }
}
