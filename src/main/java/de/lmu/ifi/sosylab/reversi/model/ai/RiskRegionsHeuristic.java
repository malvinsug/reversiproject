package de.lmu.ifi.sosylab.reversi.model.ai;

import de.lmu.ifi.sosylab.reversi.model.Cell;
import de.lmu.ifi.sosylab.reversi.model.ExperimentalAiSettings;
import de.lmu.ifi.sosylab.reversi.model.GameField;
import de.lmu.ifi.sosylab.reversi.model.Model;
import de.lmu.ifi.sosylab.reversi.model.Player;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A heuristic that prevents the AI from choosing the corner cells and encourages the AI to put
 * strategic cells.
 */
public class RiskRegionsHeuristic extends HeuristicSimple {

  public static final int FACTOR = 2;
  private static final int LOWER_BOUND_SWEET_16 = 2;
  private static final int UPPER_BOUND_SWEET_16 = 5;

  private static final int CORNER_ZERO = 0;
  private static final int CORNER_SEVEN = 7;

  private static final int REWARD_CORNER = 5;
  private static final int REWARD_SWEET16 = 2;
  private static final int REWARD_CREGION = -2;
  private static final int REWARD_XREGION = -5;

  private static final int REWARD_COVETED = 3;
  private static final int REWARD_OTHER = 1;

  private static final int INTERACTION_CONSTANT1 = 1;
  private static final int INTERACTION_CONSTANT2 = 10;
  private static final int INTERACTION_CONSTANT3 = 5;

  private int interactionConstant1 = INTERACTION_CONSTANT1;
  private int interactionConstant2 = INTERACTION_CONSTANT2;
  private int interactionConstant3 = INTERACTION_CONSTANT3;

  static Map<Cell, Integer> rewards;

  static {
    rewards = createRewardMapping();
  }

  /**
   * Constructor for the risk regions heuristic.
   *
   * @param reversi game model used for the evaluation
   * @param computer player who is played by the ai
   */
  public RiskRegionsHeuristic(Model reversi, Player computer) {
    super(reversi, computer);
  }

  /**
   * Constructor for the experimental heuristic.
   *
   * @param reversi game model used for the evaluation
   * @param computer player who is played by the ai
   * @param settings adjusted heuristics by player
   */
  public RiskRegionsHeuristic(Model reversi, Player computer, ExperimentalAiSettings settings) {
    super(reversi, computer);
    this.interactionConstant1 = settings.getInterConst1();
    this.interactionConstant2 = settings.getInterConst2();
    this.interactionConstant3 = settings.getInterConst3();
  }

  @Override
  public int heuristicEval() {
    Set<Cell> computerDisks = reversi.getState().getAllCellsOfPlayer(computer);
    Set<Cell> humanDisks = reversi.getState().getAllCellsOfPlayer(human);
    int currentDiskEval = computeEval(computerDisks) - FACTOR * computeEval(humanDisks);

    Set<Cell> computerMoves = reversi.getPossibleMovesForDisk(computer);
    Set<Cell> humanMoves = reversi.getPossibleMovesForDisk(human);
    int possibleMovesEval = computeEval(computerMoves) - FACTOR * computeEval(humanMoves);

    int eval =
        (interactionConstant3 * currentDiskEval + possibleMovesEval) * interactionConstant2
            + interactionConstant1 * super.heuristicEval();

    return eval;
  }

  /**
   * Computes the rewards for the disks.
   *
   * @param disks set for calcuating the rewards
   * @return reward
   */
  private int computeEval(Set<Cell> disks) {
    int eval = 0;
    for (Cell cell : disks) {
      eval += rewards.get(cell);
    }
    return eval;
  }

  /**
   * Creates the reward mapping.
   *
   * @return Reward Hashset
   */
  private static HashMap<Cell, Integer> createRewardMapping() {
    HashMap<Cell, Integer> rewards = new HashMap<>();
    for (int i = 0; i < GameField.SIZE; i++) {
      for (int j = 0; j < GameField.SIZE; j++) {
        Cell cell = Cell.createCell(i, j);
        if (isSweet16Cell(cell)) {
          rewards.put(cell, REWARD_SWEET16);
        } else if (isCCell(cell)) {
          rewards.put(cell, REWARD_CREGION);
        } else if (isCovetedCell(cell)) {
          rewards.put(cell, REWARD_COVETED);
        } else if (isInCell(CORNER_ZERO, CORNER_SEVEN, cell)) { // if cell is in the corner
          rewards.put(cell, REWARD_CORNER);
        } else if (isInCell(CORNER_ZERO + 1, CORNER_SEVEN - 1, cell)) { // if is in x regions
          rewards.put(cell, REWARD_XREGION);
        } else {
          rewards.put(cell, REWARD_OTHER);
        }
      }
    }
    return rewards;
  }

  /**
   * Tests if cell is in the sweet 16 area. Further explaination can be found in our wiki -> Reversi
   * AI -> Paper that inspires RiskRegionsHeuristic.java.
   *
   * @param candidate cell to test
   * @return true if cell is in 16-area
   */
  private static boolean isSweet16Cell(Cell candidate) {
    int column = candidate.getColumn();
    int row = candidate.getRow();

    boolean isColumnInSweet16Cells =
        column >= LOWER_BOUND_SWEET_16 && column <= UPPER_BOUND_SWEET_16;
    boolean isRowInSweet16Cells = row >= LOWER_BOUND_SWEET_16 && row <= UPPER_BOUND_SWEET_16;

    return isColumnInSweet16Cells && isRowInSweet16Cells;
  }

  /**
   * Tests if the cell is in certain cells.It is used for x region cells and corner cells. Further
   * explanation can be found in our wiki -> Reversi AI -> Paper that inspires
   * RiskRegionsHeuristic.java.
   *
   * @param cellIndex1 corner index
   * @param cellIndex2 corner index
   * @param candidate cell to test on
   * @return true if cell is in corner
   */
  private static boolean isInCell(int cellIndex1, int cellIndex2, Cell candidate) {
    int column = candidate.getColumn();
    int row = candidate.getRow();

    boolean isCell11 = (column == cellIndex1) && (row == cellIndex1);
    boolean isCell16 = (column == cellIndex1) && (row == cellIndex2);
    boolean isCell61 = (column == cellIndex2) && (row == cellIndex1);
    boolean isCell66 = (column == cellIndex2) && (row == cellIndex2);

    return isCell11 || isCell16 || isCell61 || isCell66;
  }

  /**
   * Tests if the cell is in the side area. The side area can benefit the player at the beginning of
   * the game because it can dominate other player. Further explanation can be found in our wiki ->
   * Reversi AI -> Paper that inspires RiskRegionsHeuristic.java.
   *
   * @param candidate cell to test
   * @return true if it is in this area
   */
  private static boolean isCovetedCell(Cell candidate) {
    int column = candidate.getColumn();
    int row = candidate.getRow();

    boolean isLeftSide = column == CORNER_ZERO && row > CORNER_ZERO + 1 && row < CORNER_SEVEN - 1;
    boolean isRightSide = column == CORNER_SEVEN && row > CORNER_ZERO + 1 && row < CORNER_SEVEN - 1;
    boolean isDownSide =
        row == CORNER_ZERO && column > CORNER_ZERO + 1 && column < CORNER_SEVEN - 1;
    boolean isTopSide =
        row == CORNER_SEVEN && column > CORNER_ZERO + 1 && column < CORNER_SEVEN - 1;

    return isLeftSide || isRightSide || isDownSide || isTopSide;
  }

  /**
   * Tests if the cell is in the C-Area. Further explaination can be found in our wiki -> Reversi AI
   * -> Paper that inspires RiskRegionsHeuristic.java.
   *
   * @param candidate cell to test
   * @return true if it is in this area
   */
  private static boolean isCCell(Cell candidate) {
    ArrayList<Cell> regionCCells = new ArrayList<>();
    regionCCells.add(Cell.createCell(CORNER_ZERO, CORNER_ZERO + 1));
    regionCCells.add(Cell.createCell(CORNER_ZERO, CORNER_SEVEN - 1));
    regionCCells.add(Cell.createCell(CORNER_ZERO + 1, CORNER_ZERO));
    regionCCells.add(Cell.createCell(CORNER_ZERO + 1, CORNER_SEVEN));
    regionCCells.add(Cell.createCell(CORNER_ZERO, CORNER_SEVEN - 1));
    regionCCells.add(Cell.createCell(CORNER_SEVEN - 1, CORNER_SEVEN));
    regionCCells.add(Cell.createCell(CORNER_SEVEN, CORNER_ZERO));
    regionCCells.add(Cell.createCell(CORNER_SEVEN, CORNER_SEVEN - 1));

    return regionCCells.contains(candidate);
  }
}
