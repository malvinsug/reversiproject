package de.lmu.ifi.sosylab.reversi.model.communication;

public class WaitingGame {

  private TcpConnection connection;
  private Pdu message;

  public WaitingGame(TcpConnection connection, Pdu message) {
    this.connection = connection;
    this.message = message;
  }

  public TcpConnection getConnection1() {
    return connection;
  }

  public void setConnection(TcpConnection connection) {
    this.connection = connection;
  }

  public Pdu getMessage() {
    return message;
  }

  public void setMessage(Pdu message) {
    this.message = message;
  }
}
