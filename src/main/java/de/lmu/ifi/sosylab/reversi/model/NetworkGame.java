package de.lmu.ifi.sosylab.reversi.model;

import de.lmu.ifi.sosylab.reversi.model.communication.Pdu;
import de.lmu.ifi.sosylab.reversi.model.communication.ReversiClient;
import java.util.HashSet;
import java.util.Set;

/** NetworkGame. */
public class NetworkGame extends Reversi {

  private ReversiClient client;

  /**
   * Create a new network Game (for client).
   *
   * @param client The client that started the game
   */
  public NetworkGame(ReversiClient client) {
    this.client = client;
    getState().setCurrentPhase(Phase.WAITING);
  }

  @Override
  public Set<Cell> getPossibleMovesForDisk(Player player) {
    if (getState().getCurrentPlayer() == client.getPlayer()) {
      return super.getPossibleMovesForDisk(player);
    } else {
      return new HashSet<Cell>();
    }
  }

  @Override
  public synchronized boolean move(Cell target) {
    GameState currentState = getState();
    if (client.getPlayer().equals(currentState.getCurrentPlayer())) {
      if (!super.move(target)) {
        return false;
      } else {
        try {
          Pdu newPdu = client.checkMove(getState());
          setState(newPdu.getState());
          return true;
        } catch (Exception e) {
          System.out.println("Exception while checking the Move");
          return false;
        }
      }
    } else {
      System.out.println("Its not your turn");
      return false;
    }
  }
}
