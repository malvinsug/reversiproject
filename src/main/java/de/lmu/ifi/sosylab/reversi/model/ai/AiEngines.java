package de.lmu.ifi.sosylab.reversi.model.ai;

/** The different AI implementations. */
public enum AiEngines {
  EASY_AI,
  MINIMAX_SIMPLE_HEURISTIC,
  MINIMAX_ADVANCED_HEURISTIC,
  EXPERIMENTAL_HEURISTIC
}
