package de.lmu.ifi.sosylab.reversi.model.communication;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.lmu.ifi.sosylab.reversi.model.GameState;
import de.lmu.ifi.sosylab.reversi.model.Player;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Model of a simple Pdu.
 *
 * @author mandlp
 */
public class Pdu implements Serializable {

  private static final long serialVersionUID = -6172619032079227589L;

  @JsonProperty("gameID")
  private int gameID;

  @JsonProperty("messageType")
  private String messageType;

  @JsonProperty("state")
  private GameState state;

  @JsonProperty("player")
  private Player player;

  @JsonProperty("mode")
  private String mode;

  @JsonProperty("waitingGames")
  private Pdu[] waitingGames;

  /**
   * Create a new Pdu.
   *
   * @param gameID the id of the game
   * @param messageType the message type
   * @param gameState the game state
   * @param player the player of the game
   * @param mode the mode
   */
  @JsonCreator
  public Pdu(
      @JsonProperty("gameID") int gameID,
      @JsonProperty("messageType") String messageType,
      @JsonProperty("state") GameState gameState,
      @JsonProperty("player") Player player,
      @JsonProperty("mode") String mode) {
    this.gameID = gameID;
    this.messageType = messageType;
    this.state = gameState;
    this.player = player;
    this.mode = mode;
  }

  /**
   * Generates a String with all the values of the message.
   *
   * @return the message in string form
   */
  public String toString() {
    String result = "";
    result += "ID : " + getGameID() + " -> ";
    result += "Player " + getPlayer();
    result += " is waiting.";
    return result;
  }

  @JsonIgnore
  public void setPlayer(Player player) {
    this.player = player;
  }

  @JsonIgnore
  public Player getPlayer() {
    return player;
  }

  @JsonIgnore
  public void setState(GameState state) {
    this.state = state;
  }

  @JsonIgnore
  public String getMode() {
    return mode;
  }

  @JsonIgnore
  public void setMessageType(String messageType) {
    this.messageType = messageType;
  }

  @JsonIgnore
  public int getGameID() {
    return gameID;
  }

  @JsonIgnore
  public GameState getState() {
    return state;
  }

  @JsonIgnore
  public String getMessageType() {
    return messageType;
  }

  /**
   * Set the waiting games.
   *
   * @param waitingGames the list of the waiting games
   */
  @JsonIgnore
  public void setWaitingGames(Pdu[] waitingGames) {
    ArrayList<Pdu> games = new ArrayList<>();
    games.addAll(Arrays.asList(waitingGames));
    this.waitingGames = games.toArray(new Pdu[games.size()]);
  }

  @JsonIgnore
  Pdu[] getWaitingGames() {
    return waitingGames;
  }

  /**
   * Convert the Pdu object into String for JSON.
   *
   * @author malvin
   * @return JSON string
   * @throws JsonProcessingException exception for mapping the object into String.
   */
  public String toJson() throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    String json = mapper.writeValueAsString(this);
    return json;
  }

  /**
   * Convert JSON String into Pdu object.
   *
   * @author malvin
   * @return Pdu unit.
   * @throws JsonProcessingException exception for mapping string into object.
   */
  public static Pdu readPdu(String json) throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    Pdu receivedPdu = mapper.readValue(json, Pdu.class);
    return receivedPdu;
  }
}
