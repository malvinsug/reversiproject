package de.lmu.ifi.sosylab.reversi.view;

import static java.util.Objects.requireNonNull;

import de.lmu.ifi.sosylab.reversi.model.Player;
import de.lmu.ifi.sosylab.reversi.model.communication.Pdu;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.InetAddress;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;

public class NetworkLobbyView extends JPanel implements PropertyChangeListener {

  private static final long serialVersionUID = -2718685123110515080L;

  private Controller controller;

  private Pdu[] openGames;
  private InetAddress address;

  NetworkLobbyView(Controller controller, Pdu[] openGames, InetAddress address) {
    this.controller = requireNonNull(controller);
    this.openGames = requireNonNull(openGames);
    this.address = requireNonNull(address);
  }

  /** Creates default Netlobby View. */
  public void createView() {
    EventQueue.invokeLater(
        () -> {
          JFrame frame = new JFrame("Lobby");
          frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
          try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
          } catch (Exception e) {
            e.printStackTrace();
          }
          JPanel panel = new JPanel();
          panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
          panel.setOpaque(true);

          DefaultListModel<Pdu> listModel = new DefaultListModel<>();
          for (Pdu openGame : openGames) {
            listModel.addElement(openGame);
          }
          JList<Pdu> matchList = new JList<>(listModel);
          matchList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          matchList.setSelectedIndex(0);
          matchList.addListSelectionListener(e -> {});

          matchList.setVisibleRowCount(10);

          JScrollPane scroller = new JScrollPane(matchList);
          scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
          scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

          JPanel inputpanel = new JPanel();
          inputpanel.setLayout(new FlowLayout());

          JButton joinButton = new JButton("Join");
          joinButton.addActionListener(
              e -> {
                Pdu selectedPdu = matchList.getSelectedValue();
                int gameId = selectedPdu.getGameID();
                String mode = selectedPdu.getMode();
                Player player = Player.getOpponentOf(selectedPdu.getPlayer());
                controller.clientNetworkStartGameSelected(address, player, mode, "join", gameId);
                frame.dispose();
              });

          panel.add(scroller);
          inputpanel.add(joinButton);
          panel.add(inputpanel);

          frame.getContentPane().add(BorderLayout.CENTER, panel);
          frame.pack();
          frame.setLocationByPlatform(true);
          frame.setVisible(true);
          frame.setResizable(true);
        });
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {}
}
