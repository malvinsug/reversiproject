package de.lmu.ifi.sosylab.reversi.view;

import static java.util.Objects.requireNonNull;

import de.lmu.ifi.sosylab.reversi.model.Model;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * The main-view class. Consists of a card-layout, such that depending on the user input the
 * requested frame can be simply exchanged by a new one.
 */
class BasicView extends JFrame implements View {

  private static final long serialVersionUID = 6320900067840491616L;

  private static final int MINIMUM_FRAME_HEIGHT = 400;
  private static final int MINIMUM_FRAME_WIDTH = 400;

  private static final String START_VIEW = "Start View";
  private static final String GAME_VIEW = "Game View";
  private static final String SETTINGS_VIEW = "Settings View";
  private static final String NETWORK_VIEW = "Network View";
  private static final String EXPERIMENTAL_SETTINGS = "Experimental Settings";

  private final Controller controller;
  private final StartView startView;

  private ReversiView reversiView;
  private SettingsView settingsView;
  private NetworkView networkView;
  private ExperimentalSettings experimentalSettings;

  private Container contentPane;
  private CardLayout cardLayout;

  BasicView(Controller controller) {
    super("Reversi");
    this.controller = requireNonNull(controller);

    setMinimumSize(new Dimension(MINIMUM_FRAME_WIDTH, MINIMUM_FRAME_HEIGHT));
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    startView = new StartView(controller);
    pack();

    initializeCardPanel();
  }

  private void initializeCardPanel() {
    setBackground(new Color(190, 188, 189));

    cardLayout = new CardLayout();
    setLayout(cardLayout);

    contentPane = getContentPane();
    contentPane.add(startView, START_VIEW);
  }

  @Override
  public void showStartMenu() {
    cardLayout.show(contentPane, START_VIEW);
    pack();
  }

  @Override
  public void showView() {
    setLocationByPlatform(true);
    pack();
    setVisible(true);
  }

  @Override
  public void showGame(Model model) {
    reversiView = new ReversiView(model, controller);
    contentPane.add(reversiView, GAME_VIEW);
    cardLayout.show(getContentPane(), GAME_VIEW);
    pack();
  }

  @Override
  public void showNetworkView() {
    if (networkView != null) {
      contentPane.add(networkView, NETWORK_VIEW);

      cardLayout.show(getContentPane(), NETWORK_VIEW);
      pack();
      setVisible(true);
    } else {
      networkView = new NetworkView((controller));
      contentPane.add(networkView, NETWORK_VIEW);

      cardLayout.show(getContentPane(), NETWORK_VIEW);
      pack();
      setVisible(true);
    }
  }

  @Override
  public void showSettings() {
    if (settingsView != null) {
      contentPane.add(settingsView, SETTINGS_VIEW);

      cardLayout.show(getContentPane(), SETTINGS_VIEW);
      pack();
      setVisible(true);
    } else {
      settingsView = new SettingsView(controller);
      contentPane.add(settingsView, SETTINGS_VIEW);

      cardLayout.show(getContentPane(), SETTINGS_VIEW);
      pack();
      setVisible(true);
    }
  }

  @Override
  public void removeGame() {
    if (reversiView != null) {
      contentPane.remove(reversiView);
      reversiView.dispose();
      reversiView = null;
    }
  }

  @Override
  public void removeSettings() {
    if (settingsView != null) {
      contentPane.remove(settingsView);
    }
  }

  @Override
  public void removeNetworkView() {
    if (networkView != null) {
      contentPane.remove(networkView);
    }
  }

  @Override
  public void showExperimentalSettings() {
    if (experimentalSettings != null) {
      contentPane.add(experimentalSettings, EXPERIMENTAL_SETTINGS);

      cardLayout.show(getContentPane(), EXPERIMENTAL_SETTINGS);
      pack();
      setVisible(true);
    } else {
      experimentalSettings = new ExperimentalSettings(controller);
      contentPane.add(experimentalSettings, EXPERIMENTAL_SETTINGS);

      cardLayout.show(getContentPane(), EXPERIMENTAL_SETTINGS);
      pack();
      setVisible(true);
    }
  }

  @Override
  public void removeExperimentalSettings() {
    if (experimentalSettings != null) {
      contentPane.remove(experimentalSettings);
    }
  }

  @Override
  public void dispose() {
    removeGame();
    controller.dispose();
    super.dispose();
  }

  @Override
  public void showErrorMessage(String message) {
    if (reversiView != null) {
      reversiView.showErrorMessage(message);
    } else {
      JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
  }
}
