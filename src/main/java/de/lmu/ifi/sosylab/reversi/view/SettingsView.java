package de.lmu.ifi.sosylab.reversi.view;

import de.lmu.ifi.sosylab.reversi.model.ai.AiEngines;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.TextAttribute;
import java.util.Map;
import java.util.Objects;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class SettingsView extends JPanel {

  private static final long serialVersionUID = -1491754184858221489L;
  private Controller controller;

  private JLabel difficultyLabel;
  private JPanel returnPanel;
  private JButton returnButton;
  private JLabel errorLabel;

  GridBagConstraints constraint = new GridBagConstraints();
  GridBagConstraints nav = new GridBagConstraints();

  /**
   * Starts Settings View.
   *
   * @param controller controller for the view
   */
  public SettingsView(Controller controller) {
    this.controller = Objects.requireNonNull(controller);

    initializeWidgets();
    createView();
  }

  private void initializeWidgets() {
    int fontsize = 24;

    difficultyLabel = new JLabel("Difficulty");
    difficultyLabel.setForeground(Color.BLACK);
    difficultyLabel.setFont(new Font(difficultyLabel.getFont().getFontName(), Font.BOLD, fontsize));
    Map difAttributes = difficultyLabel.getFont().getAttributes();
    difAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
    difficultyLabel.setFont(difficultyLabel.getFont().deriveFont(difAttributes));

    returnPanel = new JPanel(new FlowLayout());

    returnButton = new JButton("Return");
    returnButton.addActionListener(new SettingsViewListener());
    returnButton.setEnabled(true);
    errorLabel = new JLabel();
    errorLabel.setForeground(Color.RED);
    errorLabel.setFont(new Font(errorLabel.getFont().getFontName(), Font.PLAIN, fontsize));
  }

  private void createView() {
    setLayout(new GridBagLayout());

    constraint.anchor = GridBagConstraints.CENTER;
    constraint.weightx = 1.0f;
    constraint.weighty = 1.0f;
    JPanel panel = new JPanel();
    add(panel, constraint);

    panel.setLayout(new GridBagLayout());
    constraint = new GridBagConstraints();
    constraint.gridx = 0;
    constraint.ipadx = 20;
    constraint.ipady = 5;
    constraint.insets = new Insets(20, 1, 10, 10);

    panel.add(difficultyLabel, constraint);

    JComponent difficultySettings = createOptions("Difficulty");
    difficultySettings.setAlignmentX(Component.CENTER_ALIGNMENT);
    constraint.ipady = 1;
    panel.add(difficultySettings, constraint);

    JButton experimentals =
        createButton(
            "Experimental Settings",
            "Shows advanced difficulty settings to customize",
            controller::experimentalSettingsSelected);
    panel.add(experimentals, constraint);

    constraint.ipady = 5;

    constraint.ipady = 1;

    nav.anchor = GridBagConstraints.SOUTH;
    nav.weightx = 1.0f;
    nav.weighty = 1.0f;
    nav.gridx = 0;
    nav.ipadx = 10;
    nav.ipady = 90;
    nav.insets = new Insets(5, 5, 1, 20);

    JPanel navigationPanel = new JPanel(new BorderLayout());

    returnPanel.add(returnButton, CENTER_ALIGNMENT);
    navigationPanel.add(returnPanel, BorderLayout.SOUTH);
    add(navigationPanel, nav);
  }

  private JButton createButton(String name, String tooltip, Runnable action) {
    JButton button = new JButton(name);
    button.setToolTipText(tooltip);
    button.setEnabled(true);
    button.addActionListener(event -> action.run());

    return button;
  }

  private JComponent createOptions(String panel) {
    int fontsize = 15;

    JPanel difficultyOptions = new JPanel();
    difficultyOptions.setLayout(new BoxLayout(difficultyOptions, BoxLayout.X_AXIS));

    ButtonGroup aiDifficulty = new ButtonGroup();
    JRadioButton easyDifficulty = new JRadioButton("Easy");
    easyDifficulty.setFont(new Font(easyDifficulty.getFont().getFontName(), Font.PLAIN, fontsize));
    aiDifficulty.add(easyDifficulty);
    difficultyOptions.add(easyDifficulty);
    easyDifficulty.addActionListener(e -> controller.setAiDifficulty(AiEngines.EASY_AI));

    JRadioButton normalDifficulty = new JRadioButton("Normal");
    aiDifficulty.add(normalDifficulty);
    difficultyOptions.add(normalDifficulty);
    normalDifficulty.setSelected(true);
    normalDifficulty.addActionListener(
        e -> controller.setAiDifficulty(AiEngines.MINIMAX_SIMPLE_HEURISTIC));

    JRadioButton hardDifficulty = new JRadioButton("Hard");
    aiDifficulty.add(hardDifficulty);
    difficultyOptions.add(hardDifficulty);
    hardDifficulty.addActionListener(
        e -> controller.setAiDifficulty(AiEngines.MINIMAX_ADVANCED_HEURISTIC));

    JRadioButton expButton = new JRadioButton("Experimentals");
    aiDifficulty.add(expButton);
    difficultyOptions.add(expButton);
    expButton.addActionListener(
        actionEvent -> controller.setAiDifficulty(AiEngines.EXPERIMENTAL_HEURISTIC));

    JPanel colorOptions = new JPanel();
    colorOptions.setLayout(new BoxLayout(colorOptions, BoxLayout.X_AXIS));

    JPanel option = new JPanel();

    if (panel.equals("Difficulty")) {
      option = difficultyOptions;
    }
    return option;
  }

  private class SettingsViewListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      if (e.getSource() == returnButton) {
        handleReturnButtonClick();
      }
    }
  }

  private void handleReturnButtonClick() {
    controller.showStartView();
  }
}
