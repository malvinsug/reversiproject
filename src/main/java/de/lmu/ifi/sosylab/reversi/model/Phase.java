package de.lmu.ifi.sosylab.reversi.model;

/** Provides constants that represent the possible stages of this reversi game. */
public enum Phase {
  RUNNING,
  FINISHED,
  WAITING,
}
