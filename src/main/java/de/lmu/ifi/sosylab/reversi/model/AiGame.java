package de.lmu.ifi.sosylab.reversi.model;

import de.lmu.ifi.sosylab.reversi.model.ai.AiEngine;
import de.lmu.ifi.sosylab.reversi.model.ai.AiEngines;
import de.lmu.ifi.sosylab.reversi.model.ai.AiRandom;
import de.lmu.ifi.sosylab.reversi.model.ai.Minimax;
import de.lmu.ifi.sosylab.reversi.model.ai.RiskRegionsHeuristic;
import java.awt.event.ActionListener;
import javax.swing.Timer;

/** AiGame which allows to play against a AI agent. */
public class AiGame extends Reversi {

  private Player computer;
  private AiEngine aiEngine;
  private AiEngines ai;
  private ExperimentalAiSettings settings;

  /** Creates a Ai game with the default parameters. */
  public AiGame() {
    super();
    this.ai = AiEngines.MINIMAX_ADVANCED_HEURISTIC;
    setAi(ai);
    computer = Player.WHITE;
  }

  /**
   * Creates an ai with specific parameters.
   *
   * @param computer the player who is played by the computer
   * @param ai the concrete algorithm used for the ai
   */
  public AiGame(Player computer, AiEngines ai) {
    super();
    this.ai = ai;
    setAi(ai);
    this.computer = computer;
  }

  /**
   * Creates an Ai with adjustable parameters.
   *
   * @param computer the player who is played by the computer
   * @param ai the concrete algorithm used for the ai
   * @param settings settings to adjust
   */
  public AiGame(Player computer, AiEngines ai, ExperimentalAiSettings settings) {
    super();
    this.ai = ai;
    this.settings = settings;
    this.computer = computer;
    setAi(ai);
  }

  public Player getComputerRole() {
    return this.computer;
  }

  @Override
  public synchronized boolean move(Cell target) {
    if (!super.move(target)) {
      return false;
    }
    if (getState().getCurrentPlayer() == computer
        && getState().getCurrentPhase() == Phase.RUNNING) {
      aiMove();
    }
    return true;
  }

  @Override
  public synchronized void newGame() {
    super.newGame();
    if (this.ai == null) {
      this.ai = AiEngines.MINIMAX_SIMPLE_HEURISTIC;
    }
    setAi(ai);
    computer = Player.WHITE;
  }

  /** Method for the ai to make a move on the board. */
  void aiMove() {
    boolean isGameFinished = getState().getCurrentPhase() != Phase.RUNNING;
    if (isGameFinished) {
      return;
    }
    ActionListener listener =
        event -> {
          Cell target = getTarget();
          move(target);
        };
    Timer timer = new Timer(900, listener);
    timer.setRepeats(false);
    timer.start();
  }

  private Cell getTarget() {
    Cell target = aiEngine.getTarget();
    return target;
  }

  /**
   * This method sets the ai-algorithm used for the ai-game.
   *
   * @param ai is the concrete ai algorithms to use
   */
  public synchronized void setAi(AiEngines ai) {
    switch (ai) {
      case EASY_AI:
        this.aiEngine = new AiRandom(this);
        break;
      case MINIMAX_SIMPLE_HEURISTIC:
        this.aiEngine = new Minimax(this.getState());
        break;
      case MINIMAX_ADVANCED_HEURISTIC:
        this.aiEngine = new Minimax(this.getState(), new RiskRegionsHeuristic(this, computer));
        break;
      case EXPERIMENTAL_HEURISTIC:
        this.aiEngine =
            new Minimax(this.getState(), new RiskRegionsHeuristic(this, computer, settings));
        break;
      default:
        throw new IllegalArgumentException("there is no ai agent by this name");
    }
  }
}
