package de.lmu.ifi.sosylab.reversi.view;

import de.lmu.ifi.sosylab.reversi.model.Cell;
import de.lmu.ifi.sosylab.reversi.model.Player;
import de.lmu.ifi.sosylab.reversi.model.ai.AiEngines;
import de.lmu.ifi.sosylab.reversi.model.communication.Pdu;
import de.lmu.ifi.sosylab.reversi.model.communication.ReversiClient;
import java.io.IOException;
import java.net.InetAddress;

/**
 * The main controller interface of the reversi game. It takes the actions from the user and handles
 * them accordingly. This is by either invoking the necessary model-methods, or by directly telling
 * the view to change its graphical user-interface.
 */
public interface Controller {

  /**
   * Set the view that the controller will use afterwards.
   *
   * @param view The {@link View}.
   */
  void setView(View view);

  /**
   * Set the client associated with the current controller.
   *
   * @param client the client
   */
  void setClient(ReversiClient client);

  /**
   * Get the currently open network games.
   *
   * @param serverAddress the communication address
   * @param mode the mode
   * @return the open games
   */
  Pdu[] getOpenGames(InetAddress serverAddress, String mode);

  /** Initializes and starts the user interface. */
  void start();

  /** Resets a game such that the game is in its initial state afterwards. */
  void resetGame() throws IOException;

  /** Sets the start screen up on which the user can select between different game modes. */
  void showStartView();

  /** Returns the client. */
  ReversiClient getClient();

  /** Sets a hotseat game up that the user can afterwards play on. */
  void hotseatGameSelected();

  /** Sets a singleplayer game up that allows the user to play against an ai player. */
  void aiGameSelected();

  /**
   * Sets up a new client network game that allows the user to play against another player on the
   * network. Tries to connect to the communication at the given address.
   *
   * @param serverAddress communication address to connect to
   * @param player the player you want to play
   * @param mode the mode of the game
   * @param type the type of the game
   * @param id the game id
   */
  void clientNetworkStartGameSelected(
      InetAddress serverAddress, Player player, String mode, String type, int id);

  /**
   * Opens up a network game lobby that allows the users to choose between available matches to play
   * against each other.
   */
  void networkViewSelected();

  /** Opens up a settings menu for users to configure the settings of the application. */
  void settingsSelected();

  /**
   * Validates the input and in case of success asks the model to execute a step on the reversi
   * board.
   *
   * @param to The {@link Cell target cell}.
   * @return <code>true</code> if validating the input was successful, <code>false</code> otherwise.
   */
  boolean move(Cell to);

  /**
   * returns, if the actual game is a network game.
   *
   * @return <code>true</code> if the game is network game, <code>false</code> otherwise.
   */
  boolean isNetworkGame();

  /** Dispose any remaining resources. */
  void dispose();

  /**
   * Sets the AI mode for the AI game.
   *
   * <p>Currently there are 3 different ai modes implemented.
   *
   * @param ai of the game
   */
  void setAiDifficulty(AiEngines ai);

  /** Opens up the experimental difficulty settings menu. */
  void experimentalSettingsSelected();

  /**
   * update the interaction constant 1 see RiskRegionsHeuristics.
   *
   * @param interConst1 is the new interaction constant value.
   */
  void updateInterConst1(int interConst1);

  /**
   * update the interaction constant 2 see RiskRegionsHeuristics.
   *
   * @param interConst2 is the new interaction constant value.
   */
  void updateInterConst2(int interConst2);

  /**
   * update the interaction constant 1 see RiskRegionsHeuristics.
   *
   * @param interConst3 is the new interaction constant value.
   */
  void updateInterConst3(int interConst3);
}
