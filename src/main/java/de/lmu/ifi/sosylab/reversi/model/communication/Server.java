package de.lmu.ifi.sosylab.reversi.model.communication;

public interface Server {

  static final int MAX_NUMBER_OF_GAMES = 100;

  /**
   * Wait for connect of client.
   *
   * @return con connection to the client
   * @throws Exception Error while waiting
   */
  TcpConnection waitForConnection() throws Exception;

  /**
   * Create a Socket.
   *
   * @throws Exception error if something goes wrong while creating socket
   */
  void createSocket() throws Exception;
}
