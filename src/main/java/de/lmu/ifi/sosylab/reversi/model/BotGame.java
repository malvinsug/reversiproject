package de.lmu.ifi.sosylab.reversi.model;

import de.lmu.ifi.sosylab.reversi.model.ai.AiEngine;
import de.lmu.ifi.sosylab.reversi.model.ai.AiEngines;
import de.lmu.ifi.sosylab.reversi.model.ai.AiRandom;

/**
 * BotGame which lets 2 AI play against eachother.
 *
 * <p>THIS FEATURE IS NOT OFFICIALLY SUPPORTED. It was a way to test how our AI plays depending on
 * the parameters. Later we thought we could use it to show a full ai game in the gui but we had not
 * the time to due it properly.
 */
public class BotGame extends AiGame {

  private AiEngine engine2;

  /** Creates a game that lets 2 AIs play against each other. */
  BotGame() {
    super(Player.WHITE, AiEngines.MINIMAX_SIMPLE_HEURISTIC);
    // engine2 = new Minimax(getState());
    // engine2.setComputer(Player.BLACK);
    engine2 = new AiRandom(this);
  }

  /** both Ais make one move. */
  public void move() {
    Cell target = engine2.getTarget();
    super.move(target);
  }

  /** Plays the game till it is finished. */
  public void fullGame() {
    while (this.getState().getCurrentPhase() == Phase.RUNNING) {
      move();
    }
  }

  /**
   * Runns 2 AIs against eachother and outputs the statistics of them.
   *
   * @param args does nothing
   */
  public static void main(String[] args) {
    int winsBlack = 0;
    int winsWhite = 0;
    int draws = 0;
    int max = 1000;
    for (int i = 0; i < max; i++) {
      BotGame game = new BotGame();
      game.fullGame();

      if (game.getState().getWinner().isPresent()) {
        if (game.getState().getWinner().get() == Player.BLACK) {
          winsBlack++;
        } else {
          winsWhite++;
        }
      } else {
        draws++;
      }
      if (i % 50 == 0) {

        System.out.println(
            "Player Back won " + winsBlack + "win chance " + ((double) (winsBlack) / max));
        System.out.println(
            "Player White won " + winsWhite + "win chance " + ((double) (winsWhite) / max));
        System.out.println("Player draws " + draws);
        System.out.println("win-los " + (winsWhite - winsBlack));
      }
    }
    System.out.println("Final:\n");
    System.out.println(
        "Player Back won " + winsBlack + "win chance " + ((double) (winsBlack) / max));
    System.out.println(
        "Player White won " + winsWhite + "win chance " + ((double) (winsWhite) / max));
    System.out.println("Player draws " + draws);
    System.out.println("win-los " + (winsWhite - winsBlack));
  }
}
