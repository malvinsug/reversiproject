package de.lmu.ifi.sosylab.reversi.model.communication;

import de.lmu.ifi.sosylab.reversi.model.GameState;

public interface Client {

  /**
   * Method, to connect to the communication.
   *
   * @param remoteServerAddress address of the communication
   * @param serverPort the communication port
   * @param localPort the local port
   * @param sendBufferSize bufferSize for sending
   * @param receiveBufferSize buffer size for receiving
   * @return TcpConnection the connection instance
   * @throws Exception error, if something goes wrong, while connection to communication.
   */
  TcpConnection connectToServer(
      String remoteServerAddress,
      int serverPort,
      int localPort,
      int sendBufferSize,
      int receiveBufferSize)
      throws Exception;

  /**
   * close the connection.
   *
   * @throws Exception error, if something goes wrong while closing the connection
   */
  void close() throws Exception;

  /**
   * Reset a network game.
   *
   * @param state the state you send to the communication
   * @throws Exception error
   */
  void resetGame(GameState state) throws Exception;

  /**
   * Delete a network game.
   *
   * @throws Exception error
   */
  void removeGame() throws Exception;

  /**
   * Send state, then wait until receiving an answer.
   *
   * @param state the actual state, to be synched.
   * @param type the message_type (join/start/update)
   * @throws Exception error, if something goes wrong while handling request/responses.
   */
  void syncState(GameState state, String type) throws Exception;

  /**
   * check the move and update the game if the move is ok.
   *
   * @throws Exception error, if something goes wrong while handling request.
   */
  Pdu checkMove(GameState state) throws Exception;

  /**
   * Wait to recieve a message.
   *
   * @throws Exception error, if something goes wrong while handling the receive.
   */
  Pdu receive() throws Exception;
}
