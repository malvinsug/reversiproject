package de.lmu.ifi.sosylab.reversi.model.ai;

import de.lmu.ifi.sosylab.reversi.model.Cell;
import de.lmu.ifi.sosylab.reversi.model.Player;

/** Public interface all AI agents need to implement. */
public interface AiEngine {

  /**
   * Method to get the target for the ai move.
   *
   * @return cell where the ai wants to move to
   */
  public Cell getTarget();

  /**
   * Sets which player is the computer.
   *
   * @param computer is the player for which the ai should make a move
   */
  public void setComputer(Player computer);

  /**
   * Sets the heuristic that should be used.
   *
   * @param heuristic the cuncret heuristic that should be used
   */
  public void setHeuristic(Heuristic heuristic);
}
