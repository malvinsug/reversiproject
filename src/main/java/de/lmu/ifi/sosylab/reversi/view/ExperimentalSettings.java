package de.lmu.ifi.sosylab.reversi.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.TextAttribute;
import java.util.Map;
import java.util.Objects;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ExperimentalSettings extends SettingsView {

  private static final long serialVersionUID = 304493663738153820L;
  private Controller controller;

  private JLabel slider1Label;
  private JLabel slider2Label;
  private JLabel slider3Label;
  private JLabel slidersDescription;
  private JPanel returnPanel;
  private JButton returnButton;
  private JLabel errorLabel;

  static final int FRONTSIZE = 15;
  static final int DESCRIPTION_FRONT_SIZE = 14;

  ExperimentalSettings(Controller controller) {
    super(controller);
    this.controller = Objects.requireNonNull(controller);

    initializeWidgets();
    createView();
  }

  private void initializeWidgets() {

    slider1Label = new JLabel("Number of Conquered Cells");
    slider1Label.setForeground(Color.BLACK);
    slider1Label.setFont(new Font(slider1Label.getFont().getFontName(), Font.BOLD, FRONTSIZE));
    Map slider1Attributes = slider1Label.getFont().getAttributes();
    slider1Attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
    slider1Label.setFont(slider1Label.getFont().deriveFont(slider1Attributes));

    slider2Label = new JLabel("Player Disk's Position");
    slider2Label.setForeground(Color.BLACK);
    slider2Label.setFont(new Font(slider2Label.getFont().getFontName(), Font.BOLD, FRONTSIZE));
    Map slider2Attributes = slider2Label.getFont().getAttributes();
    slider2Attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
    slider2Label.setFont(slider1Label.getFont().deriveFont(slider2Attributes));

    slider3Label = new JLabel("AI Disk's Position");
    slider3Label.setForeground(Color.BLACK);
    slider3Label.setFont(new Font(slider3Label.getFont().getFontName(), Font.BOLD, FRONTSIZE));
    Map slider3Attributes = slider3Label.getFont().getAttributes();
    slider3Attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
    slider3Label.setFont(slider1Label.getFont().deriveFont(slider3Attributes));
    slider3Label.setHorizontalTextPosition(SwingConstants.CENTER);

    slidersDescription =
        new JLabel("(Note: The bigger the number you choose is, the harder the AI gets.)");
    slidersDescription.setForeground(Color.BLACK);
    slidersDescription.setFont(
        new Font(slidersDescription.getFont().getFontName(), Font.PLAIN, DESCRIPTION_FRONT_SIZE));

    returnPanel = new JPanel(new FlowLayout());
    returnButton = new JButton("Return");
    returnButton.addActionListener(new ExperimentalSettingsListener());
    returnButton.setEnabled(true);
    errorLabel = new JLabel();
    errorLabel.setForeground(Color.RED);
    errorLabel.setFont(new Font(errorLabel.getFont().getFontName(), Font.PLAIN, FRONTSIZE));
  }

  private void createView() {
    setLayout(new BorderLayout());

    JPanel sliders = new JPanel();
    sliders.setLayout(new BoxLayout(sliders, BoxLayout.PAGE_AXIS));
    sliders.add(Box.createRigidArea(new Dimension(0, 20)));

    JSlider difficultySlider1 = new JSlider(JSlider.HORIZONTAL, 0, 20, 0);
    difficultySlider1.setMajorTickSpacing(5);
    difficultySlider1.setMinorTickSpacing(1);
    difficultySlider1.setPaintTicks(true);
    difficultySlider1.setPaintLabels(true);
    difficultySlider1.setSnapToTicks(true);
    difficultySlider1.addChangeListener(new SliderListener(1));

    JSlider difficultySlider2 = new JSlider(JSlider.HORIZONTAL, 0, 20, 0);
    difficultySlider2.setMajorTickSpacing(5);
    difficultySlider2.setMinorTickSpacing(1);
    difficultySlider2.setPaintTicks(true);
    difficultySlider2.setPaintLabels(true);
    difficultySlider2.setSnapToTicks(true);
    difficultySlider2.addChangeListener(new SliderListener(2));

    JSlider difficultySlider3 = new JSlider(JSlider.HORIZONTAL, 0, 20, 0);
    difficultySlider3.setMajorTickSpacing(5);
    difficultySlider3.setMinorTickSpacing(1);
    difficultySlider3.setPaintTicks(true);
    difficultySlider3.setPaintLabels(true);
    difficultySlider3.setSnapToTicks(true);
    difficultySlider3.addChangeListener(new SliderListener(3));

    Box.Filler filler =
        new Box.Filler(new Dimension(0, 0), new Dimension(0, 5), new Dimension(0, 15));

    sliders.add(slider1Label, RIGHT_ALIGNMENT);
    sliders.add(filler);
    sliders.add(difficultySlider1);
    sliders.add(filler);
    sliders.add(slider2Label);
    sliders.add(filler);
    sliders.add(difficultySlider2);
    sliders.add(filler);
    sliders.add(slider3Label);
    sliders.add(filler);
    sliders.add(difficultySlider3);

    sliders.add(slidersDescription);

    add(sliders, BorderLayout.CENTER);

    returnPanel.add(returnButton, CENTER_ALIGNMENT);
    add(returnPanel, BorderLayout.SOUTH);
  }

  private class SliderListener implements ChangeListener {
    private int interactionConstant;

    SliderListener(int interactionConstant) {
      this.interactionConstant = interactionConstant;
    }

    @Override
    public void stateChanged(ChangeEvent e) {
      JSlider source = (JSlider) e.getSource();
      if (!source.getValueIsAdjusting()) {
        int value = source.getValue();
        if (interactionConstant == 1) {
          controller.updateInterConst1(value);
        } else if (interactionConstant == 2) {
          controller.updateInterConst2(value);
        } else if (interactionConstant == 3) {
          controller.updateInterConst3(value);
        }
      }
    }
  }

  private class ExperimentalSettingsListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      if (e.getSource() == returnButton) {
        handleReturnButtonClick();
      }
    }
  }

  private void handleReturnButtonClick() {
    controller.settingsSelected();
  }
}
