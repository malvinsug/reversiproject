package de.lmu.ifi.sosylab.reversi.model.ai;

import com.google.common.collect.Iterables;
import de.lmu.ifi.sosylab.reversi.model.Cell;
import de.lmu.ifi.sosylab.reversi.model.GameState;
import de.lmu.ifi.sosylab.reversi.model.Model;
import de.lmu.ifi.sosylab.reversi.model.Player;
import de.lmu.ifi.sosylab.reversi.model.Reversi;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/** This class will execute the minimax algorithm. */
public class Minimax implements AiEngine {
  private int searchDeep = 5;
  private GameState state;
  private Reversi reversi;
  private Player computer = Player.WHITE;
  private Cell target;
  private Heuristic heuristic;
  private Set<Cell> possibleMoves;
  private AiEngine random;

  /**
   * creates minimax with the default heuristic.
   *
   * @param state is the current gamestate
   */
  public Minimax(GameState state) {
    this.state = state;
    this.reversi = new Reversi(state);
    this.heuristic = new HeuristicSimple(reversi, computer);
    this.random = new AiRandom(reversi);
  }

  /**
   * Creates a minimax instance with a certain heuristic.
   *
   * @param state is the current gamestate
   * @param heuristic that is used for the utility value
   */
  public Minimax(GameState state, Heuristic heuristic) {
    this(state);
    setHeuristic(heuristic);
  }

  /**
   * Computes the target the ai wants to move to.
   *
   * @return target cell for the move
   */
  Cell computeMove() {
    reversi.setState(state);
    this.possibleMoves = reversi.getPossibleMovesForDisk(computer);
    if (state.getMoveCounter() < 4) {
      return random.getTarget();
    }

    List<GameState> states = new ArrayList<>();
    states.add(state);
    executeMinimax(0, Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE, states);
    reversi.setState(state);

    if (this.target == null) {
      setTarget(random.getTarget());
    }
    return this.target;
  }

  private Integer executeMinimax(
      int deep, Integer value, Integer alpha, Integer beta, List<GameState> states) {

    GameState state = Iterables.getLast(states);
    GameState copyState = state.makeCopy();

    states.add(copyState);
    this.reversi.setState(copyState);

    Player currentPlayer = reversi.getState().getCurrentPlayer();

    Integer currentDeep = deep;
    boolean isMaxSearchDeepReached = currentDeep == searchDeep;

    if (isMaxSearchDeepReached) {
      states.remove(Iterables.getLast(states));
      this.reversi.setState(Iterables.getLast(states));
      return utilityEval(reversi);
    }

    for (Cell cell : reversi.getPossibleMovesForDisk(currentPlayer)) {
      reversi.move(cell);

      if (currentPlayer == computer) {
        // max
        int oldValue = value;
        value = Integer.max(executeMinimax(deep + 1, value, alpha, beta, states), value);
        alpha = Integer.max(alpha, value);

        if (oldValue <= value && possibleMoves.contains(cell)) {
          setTarget(cell);
        }

        if (beta >= alpha) {
          break;
        }
      }

      if (currentPlayer != computer) {
        // min

        value = Integer.min(value, executeMinimax(deep + 1, value, alpha, beta, states));
        beta = Integer.min(beta, value);

        if (beta <= alpha) {
          break;
        }
      }
      states.remove(Iterables.getLast(states));
      this.reversi.setState(Iterables.getLast(states));
    }

    return value;
  }

  private void setTarget(Cell cell) {
    this.target = cell;
  }

  private int utilityEval(Model reversi) {
    heuristic.setModel(reversi);
    return heuristic.heuristicEval();
  }

  @Override
  public Cell getTarget() {

    return computeMove();
  }

  @Override
  public void setComputer(Player computer) {
    this.computer = computer;
  }

  public void setHeuristic(Heuristic heuristic) {
    this.heuristic = heuristic;
  }
}
