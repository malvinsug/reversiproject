package de.lmu.ifi.sosylab.reversi.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Objects;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * StartView of the game that provides buttons for choosing between the different game modes of the
 * game.
 */
class StartView extends JPanel {

  private static final long serialVersionUID = -5165241820938022576L;

  private static final int PREFERRED_FRAME_WIDTH = 500;
  private static final int PREFERRED_FRAME_HEIGHT = 500;

  private final Controller controller;
  private JLabel reversiLabel;
  private static final Color REVERSI_GREEN = new Color(0, 153, 76);

  /**
   * Creates a new starting view in which all required elements are setup accordingly.
   *
   * @param controller The controller used to forward any events registered by the buttons.
   */
  StartView(Controller controller) {
    this.controller = Objects.requireNonNull(controller);

    setPreferredSize(new Dimension(PREFERRED_FRAME_WIDTH, PREFERRED_FRAME_HEIGHT));

    initializeWidgets();
    createContent();
  }

  private void initializeWidgets() {
    int fontsize = 45;

    reversiLabel = new JLabel("REVERSI");
    reversiLabel.setForeground(REVERSI_GREEN);
    reversiLabel.setFont(new Font(reversiLabel.getFont().getFontName(), Font.BOLD, fontsize));
  }

  private void createContent() {
    setLayout(new GridBagLayout());

    JPanel panel = new JPanel();
    panel.setPreferredSize(new Dimension(500, 500));

    GridBagConstraints c = new GridBagConstraints();
    c.anchor = GridBagConstraints.CENTER;
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1.0f;
    c.weighty = 1.0f;
    add(panel, c);

    panel.setLayout(new GridBagLayout());
    c = new GridBagConstraints();
    c.gridx = 0;
    c.ipadx = 30;
    c.ipady = 14;
    c.insets = new Insets(25, 30, 10, 10);

    panel.add(reversiLabel, c);

    c.insets = new Insets(25, 10, 10, 10);

    JButton hotseatGameButton =
        createButton("  Hotseat Game ", "Starts an offline game", controller::hotseatGameSelected);
    panel.add(hotseatGameButton, c);

    JButton singleplayerGameButton =
        createButton(
            "Computer Game", "Starts an offline game with an AI", controller::aiGameSelected);
    panel.add(singleplayerGameButton, c);

    JButton networkMenu =
        createButton(
            "      Network      ",
            "Opens up a menu for network games",
            controller::networkViewSelected);
    panel.add(networkMenu, c);

    JButton settingsButton =
        createButton("Settings", "Shows settings menu", controller::settingsSelected);
    panel.add(settingsButton, c);
  }

  private JButton createButton(String name, String tooltip, Runnable action) {
    JButton button = new JButton(name);
    button.setToolTipText(tooltip);
    button.setEnabled(true);
    button.addActionListener(event -> action.run());

    return button;
  }
}
