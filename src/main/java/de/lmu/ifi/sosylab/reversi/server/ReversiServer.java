package de.lmu.ifi.sosylab.reversi.server;

import de.lmu.ifi.sosylab.reversi.model.Player;
import de.lmu.ifi.sosylab.reversi.model.communication.GameThread;
import de.lmu.ifi.sosylab.reversi.model.communication.Pdu;
import de.lmu.ifi.sosylab.reversi.model.communication.Server;
import de.lmu.ifi.sosylab.reversi.model.communication.TcpConnection;
import de.lmu.ifi.sosylab.reversi.model.communication.TcpSocket;
import de.lmu.ifi.sosylab.reversi.model.communication.WaitingGame;
import java.util.ArrayList;

/** The Reversi Server. For now just one game at a time */
public class ReversiServer implements Server {

  private TcpSocket serverSocket = null;

  private static final WaitingGame[] matchLobby = new WaitingGame[MAX_NUMBER_OF_GAMES];
  private static final ArrayList<WaitingGame> randomLobby = new ArrayList<>();
  private static ArrayList<GameThread> games = new ArrayList<>();

  /**
   * The main method to start the communication.
   *
   * @param args starting arguments.
   */
  public static void main(String[] args) {

    System.out.println("Server started");

    ReversiServer server = new ReversiServer();

    try {
      server.createSocket();
      while (true) {

        System.out.println("Server is waiting for a new Player!");
        TcpConnection con1 = server.waitForConnection();

        String startMessage = (String) con1.receive();
        Pdu startPdu = Pdu.readPdu(startMessage);
        server.handleNewConnection(con1, startPdu);
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Handle New Connection of a client.
   *
   * @param connection The connection to the actual client
   * @param message the start message of the client
   */
  private void handleNewConnection(TcpConnection connection, Pdu message) {
    WaitingGame waitingGame = new WaitingGame(connection, message);
    String mode = message.getMode();
    String messageType = message.getMessageType();
    Player player = message.getPlayer();

    // client wants a matching game
    switch (mode) {
      case "match":
        // client wants to start a matching game
        switch (messageType) {
          case "start":
            createNewWaitingGame(message, waitingGame, connection);
            break;
            // client wants to join a matching game
          case "join":
            joinWaitingGame(message, waitingGame, connection, player);
            break;
            // client wants a list of games
          case "games":
            returnListOfGames(connection, message, "match");
            break;
          case "deleteGame":
            deleteGame(message.getGameID(), mode, connection);
            break;
          default:
            System.out.println("The received message is not like expected. return empty pdu");
            try {
              connection.send(new Pdu(0, null, null, null, null));
            } catch (Exception e) {
              e.printStackTrace();
            }
        }
        break;
        // client wants a random game
      case "random":
        // client wants to start a random game
        switch (messageType) {
          case "start":
            randomLobby.add(waitingGame);
            try {
              connection.send(message.toJson());
            } catch (Exception e) {
              e.printStackTrace();
            }
            break;
            // client wants to join a random game
          case "join":
            joinRandomGame(message, waitingGame, connection, player);
            break;
          case "games":
            returnListOfGames(connection, message, "random");
            break;
          default:
            System.out.println("The received message is not like expected. return empty pdu");
            try {
              connection.send(new Pdu(0, null, null, null, null));
            } catch (Exception e) {
              e.printStackTrace();
            }
        }
        break;
      case "reset":
      default:
        System.out.println("The received message is not like expected. return empty pdu");
        try {
          connection.send(new Pdu(0, null, null, null, null));
        } catch (Exception e) {
          e.printStackTrace();
        }
    }
  }

  private void deleteGame(int gameID, String mode, TcpConnection connection) {
    if (mode.matches("random")) {
      for (WaitingGame game : randomLobby) {
        if (game.getMessage().getGameID() == gameID) {
          try {
            connection.close();
          } catch (Exception e) {
            e.printStackTrace();
          }
          randomLobby.remove(game);
        }
      }
    } else {
      for (int i = 0; i < matchLobby.length; i++) {
        if (matchLobby[i].getMessage().getGameID() == gameID) {
          try {
            connection.close();
          } catch (Exception e) {
            e.printStackTrace();
          }
          matchLobby[i] = null;
        }
      }
    }
  }

  /**
   * Join a certain one of the waiting Games.
   *
   * @param message the message of the client
   * @param waitingGame the waiting game
   * @param connection the new connection
   * @param player the joining player
   */
  private void joinWaitingGame(
      Pdu message, WaitingGame waitingGame, TcpConnection connection, Player player) {
    WaitingGame match = matchLobby[message.getGameID()];
    message.setMessageType("start");
    if (match != null) {
      if (match.getMessage().getPlayer() != player) {
        if (player.equals(Player.BLACK)) {
          newGame(waitingGame.getConnection1(), match.getConnection1(), message);
          matchLobby[message.getGameID()] = null;
        } else {
          newGame(match.getConnection1(), waitingGame.getConnection1(), message);
          matchLobby[message.getGameID()] = null;
        }
      } else {
        System.out.println("Both players cannot be the same.");
        try {
          connection.close();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    } else {
      try {
        connection.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
      System.out.println("This match does not exist");
    }
  }

  private void returnListOfGames(TcpConnection connection, Pdu message, String mode) {
    ArrayList<Pdu> waitingGames = new ArrayList<>();
    try {
      if (mode.matches("match")) {
        for (WaitingGame game : matchLobby) {
          if (game != null) {
            waitingGames.add(game.getMessage());
          }
        }
      } else {
        for (WaitingGame game : randomLobby) {
          waitingGames.add(game.getMessage());
        }
      }
      message.setWaitingGames(waitingGames.toArray(new Pdu[waitingGames.size()]));
      connection.send(message.toJson());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Join a random one of the waiting Games.
   *
   * @param message the message of the client
   * @param waitingGame the waiting game
   * @param connection the new connection
   */
  private void joinRandomGame(
      Pdu message, WaitingGame waitingGame, TcpConnection connection, Player player) {
    WaitingGame match = getMatchingGame(waitingGame);
    message.setMessageType("start");
    if (match != null) {
      if (player.equals(Player.BLACK)) {
        newGame(waitingGame.getConnection1(), match.getConnection1(), message);
        randomLobby.remove(match);
      } else {
        newGame(match.getConnection1(), waitingGame.getConnection1(), message);
        randomLobby.remove(match);
      }
    } else {
      try {
        connection.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
      System.out.println("No match to join.");
    }
  }

  /**
   * Add a new Game to the List of waiting Games.
   *
   * @param message the message of the client
   * @param waitingGame the waiting game
   * @param connection the new connection
   */
  private void createNewWaitingGame(
      Pdu message, WaitingGame waitingGame, TcpConnection connection) {
    if (matchLobby[message.getGameID()] == null) {
      matchLobby[message.getGameID()] = waitingGame;
      try {
        connection.send(message.toJson());
      } catch (Exception e) {
        e.printStackTrace();
      }
    } else {
      try {
        System.out.println("This GameID is already used.");
        connection.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  private WaitingGame getMatchingGame(WaitingGame newGame) {
    Pdu message = newGame.getMessage();
    for (WaitingGame match : randomLobby) {
      if (match != null) {
        if (match.getMessage().getPlayer() != message.getPlayer()) {
          return match;
        }
      }
    }
    return null;
  }

  private void newGame(TcpConnection con1, TcpConnection con2, Pdu invite) {
    GameThread gameThread = new GameThread(con1, con2, invite);
    gameThread.start();
    games.add(gameThread);
  }

  /**
   * create Server-Socket.
   *
   * @throws Exception error
   */
  @Override
  public void createSocket() throws Exception {
    try {
      serverSocket = new TcpSocket(50000, 400000, 400000);
    } catch (Exception e) {
      throw new Exception();
    }
  }

  @Override
  public TcpConnection waitForConnection() throws Exception {
    try {
      return serverSocket.accept();
    } catch (Exception e) {
      throw new Exception();
    }
  }
}
