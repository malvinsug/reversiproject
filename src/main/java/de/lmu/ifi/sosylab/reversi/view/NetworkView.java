package de.lmu.ifi.sosylab.reversi.view;

import static java.util.Objects.requireNonNull;

import de.lmu.ifi.sosylab.reversi.model.Player;
import de.lmu.ifi.sosylab.reversi.model.communication.Pdu;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.InetAddress;
import java.net.UnknownHostException;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class NetworkView extends JPanel implements PropertyChangeListener {

  private static final long serialVersionUID = -6422648170048750287L;

  private Controller controller;

  private Pdu[] openGames;

  private JPanel returnPanel;
  private JButton returnButton;

  NetworkView(Controller controller) {
    this.controller = requireNonNull(controller);

    initializeWidgets();
    createView();
  }

  private void initializeWidgets() {
    returnPanel = new JPanel(new FlowLayout());

    returnButton = new JButton("Return");
    returnButton.addActionListener(new NetworkViewListener());
    returnButton.setEnabled(true);
  }

  private void createView() {
    setLayout(new GridBagLayout());

    JPanel panel = new JPanel();
    panel.setPreferredSize(new Dimension(500, 500));

    GridBagConstraints c = new GridBagConstraints();
    c.anchor = GridBagConstraints.CENTER;
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1.0f;
    c.weighty = 1.0f;
    add(panel, c);

    panel.setLayout(new GridBagLayout());
    c = new GridBagConstraints();
    c.gridx = 0;
    c.ipadx = 30;
    c.ipady = 14;
    c.insets = new Insets(25, 10, 10, 10);

    JPanel networkButtons = new JPanel();
    networkButtons.setLayout(new BoxLayout(networkButtons, BoxLayout.X_AXIS));

    ButtonGroup blackOrWhite = new ButtonGroup();
    JRadioButton blackButton = new JRadioButton("Black");
    blackButton.setSelected(true);
    blackOrWhite.add(blackButton);
    networkButtons.add(blackButton);

    JRadioButton whiteButton = new JRadioButton("White");

    blackOrWhite.add(whiteButton);
    networkButtons.add(whiteButton);

    ButtonGroup matchOrRandom = new ButtonGroup();
    JRadioButton matchButton = new JRadioButton("Match");
    matchButton.setSelected(true);
    matchOrRandom.add(matchButton);
    networkButtons.add(matchButton);

    JRadioButton randomButton = new JRadioButton("Random");

    matchOrRandom.add(randomButton);
    networkButtons.add(randomButton);

    JPanel clientConfig = new JPanel(new FlowLayout(FlowLayout.LEFT));
    clientConfig.setAlignmentX(Component.CENTER_ALIGNMENT);
    JLabel serverAddressLabel = new JLabel("Communication address: ");
    clientConfig.add(serverAddressLabel);
    JTextField serverAddress = new JTextField("127.0.0.1", 20);
    clientConfig.add(serverAddress);
    JPanel gameConfig = new JPanel(new FlowLayout(FlowLayout.LEFT));
    gameConfig.setAlignmentX(Component.CENTER_ALIGNMENT);
    JLabel gameIdLabel = new JLabel("Game ID: ");
    gameConfig.add(gameIdLabel);
    JTextField gameId = new JTextField("1", 2);
    gameConfig.add(gameId);
    JPanel listConfig = new JPanel(new FlowLayout(FlowLayout.LEFT));
    listConfig.setAlignmentX(Component.CENTER_ALIGNMENT);

    matchButton.addActionListener(actionEvent -> gameConfig.setVisible(true));
    randomButton.addActionListener(actionEvent -> gameConfig.setVisible(false));

    JButton networkStartButton =
        createButton(
            "Start Network Game",
            "Start a network game",
            () -> {
              try {
                InetAddress address = InetAddress.getByName(serverAddress.getText());
                int id = Integer.parseInt(gameId.getText());
                if (0 < id && id <= 99) {
                  if (blackButton.isSelected()) {
                    if (matchButton.isSelected()) {
                      controller.clientNetworkStartGameSelected(
                          address, Player.BLACK, "match", "start", id);
                    } else {
                      controller.clientNetworkStartGameSelected(
                          address, Player.BLACK, "random", "start", id);
                    }
                  } else {
                    if (matchButton.isSelected()) {
                      controller.clientNetworkStartGameSelected(
                          address, Player.WHITE, "match", "start", id);
                    } else {
                      controller.clientNetworkStartGameSelected(
                          address, Player.WHITE, "random", "start", id);
                    }
                  }
                } else {
                  JOptionPane.showMessageDialog(
                      null,
                      "The Game ID must be between 0 und 100. ",
                      "Error",
                      JOptionPane.ERROR_MESSAGE);
                }

              } catch (UnknownHostException e) {
                // may also be put in controller class; we put this here so that we can easily move
                // the focus on the communication-address text field if the address is invalid
                JOptionPane.showMessageDialog(
                    null,
                    "Invalid communication address: " + serverAddress.getText(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
                // let text field request focus after error was shown,
                // so that user can immediately fix the communication address
                serverAddress.requestFocus();
              }
            });

    JButton openGamesButton =
        createButton(
            "   Get open Games   ",
            "Get the open network games",
            () -> {
              try {
                InetAddress address = InetAddress.getByName(serverAddress.getText());
                if (matchButton.isSelected()) {
                  openGames = controller.getOpenGames(address, "match");
                } else {
                  openGames = controller.getOpenGames(address, "random");
                }
                NetworkLobbyView lobbyView = new NetworkLobbyView(controller, openGames, address);
                lobbyView.createView();
              } catch (UnknownHostException e) {
                // may also be put in controller class; we put this here so that we can easily move
                // the focus on the communication-address text field if the address is invalid
                JOptionPane.showMessageDialog(
                    null,
                    "Invalid communication address: " + serverAddress.getText(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
                // let text field request focus after error was shown,
                // so that user can immediately fix the communication address
                serverAddress.requestFocus();
              }
            });

    returnPanel.add(returnButton, CENTER_ALIGNMENT);

    JButton networkJoinButton =
        createButton(
            "Join Network Game ",
            "Join an existing network game through a matchmaking lobby",
            () -> {
              try {
                extractedNetwork(blackButton, matchButton, serverAddress, gameId);
              } catch (UnknownHostException e) {
                // may also be put in controller class; we put this here so that we can easily move
                // the focus on the communication-address text field if the address is invalid
                JOptionPane.showMessageDialog(
                    null,
                    "Invalid communication address: " + serverAddress.getText(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
                // let text field request focus after error was shown,
                // so that user can immediately fix the communication address
                serverAddress.requestFocus();
              }
            });

    panel.add(networkStartButton, c);
    panel.add(openGamesButton, c);
    panel.add(networkJoinButton, c);
    panel.add(networkButtons, c);
    panel.add(clientConfig, c);
    panel.add(gameConfig, c);
    c.insets = new Insets(2, 10, 5, 10);
    panel.add(returnPanel, c);
  }

  private void extractedNetwork(
      JRadioButton blackButton,
      JRadioButton matchButton,
      JTextField serverAddress,
      JTextField gameId)
      throws UnknownHostException {
    InetAddress address = InetAddress.getByName(serverAddress.getText());
    if (blackButton.isSelected()) {
      if (matchButton.isSelected()) {
        int id = Integer.parseInt(gameId.getText());
        controller.clientNetworkStartGameSelected(address, Player.BLACK, "match", "join", id);
      } else {
        int id = 0;
        controller.clientNetworkStartGameSelected(address, Player.BLACK, "random", "join", id);
      }
    } else {
      if (matchButton.isSelected()) {
        int id = Integer.parseInt(gameId.getText());
        controller.clientNetworkStartGameSelected(address, Player.WHITE, "match", "join", id);
      } else {
        int id = 0;
        controller.clientNetworkStartGameSelected(address, Player.WHITE, "random", "join", id);
      }
    }
  }

  private JButton createButton(String name, String tooltip, Runnable action) {
    JButton button = new JButton(name);
    button.setToolTipText(tooltip);
    button.setEnabled(true);
    button.addActionListener(event -> action.run());

    return button;
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {}

  public class NetworkViewListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      if (e.getSource() == returnButton) {
        handleReturnButtonClick();
      }
    }
  }

  private void handleReturnButtonClick() {
    controller.showStartView();
  }
}
