package de.lmu.ifi.sosylab.reversi.view;

import static java.util.Objects.requireNonNull;

import de.lmu.ifi.sosylab.reversi.model.AiGame;
import de.lmu.ifi.sosylab.reversi.model.Cell;
import de.lmu.ifi.sosylab.reversi.model.ExperimentalAiSettings;
import de.lmu.ifi.sosylab.reversi.model.GameField;
import de.lmu.ifi.sosylab.reversi.model.GameState;
import de.lmu.ifi.sosylab.reversi.model.Model;
import de.lmu.ifi.sosylab.reversi.model.NetworkGame;
import de.lmu.ifi.sosylab.reversi.model.Phase;
import de.lmu.ifi.sosylab.reversi.model.Player;
import de.lmu.ifi.sosylab.reversi.model.Reversi;
import de.lmu.ifi.sosylab.reversi.model.ai.AiEngines;
import de.lmu.ifi.sosylab.reversi.model.communication.Pdu;
import de.lmu.ifi.sosylab.reversi.model.communication.ReversiClient;
import java.io.IOException;
import java.net.InetAddress;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingWorker;

/**
 * Implementation of the controller interface which handles the interaction between the main model-
 * and view-classes. Its main purpose is to check the validity of all incoming requests and to take
 * the appropriate actions afterwards.
 */
public class ReversiController implements Controller {

  private Model model;
  private ExperimentalAiSettings settings;

  private View view;
  private AiEngines aiDifficulty = AiEngines.MINIMAX_SIMPLE_HEURISTIC;
  private Player computer = Player.WHITE;
  private ReversiClient client;
  private boolean isNetworkGame = false;
  private Thread receiverThread;

  private SwingWorker<Boolean, Void> moveWorker;

  /** Creates a controller object for a given model. */
  public ReversiController() {
    view = new BasicView(this);
    settings = new ExperimentalAiSettings(0, 0, 0);
  }

  @Override
  public void setView(View view) {
    this.view = requireNonNull(view);
  }

  @Override
  public void start() {
    view.showStartMenu();
    view.showView();
  }

  @Override
  public void showStartView() {
    view.removeGame();
    leaveCurrentGame();
    view.removeExperimentalSettings();
    view.removeSettings();
    view.removeNetworkView();
    view.showStartMenu();
  }

  private void leaveCurrentGame() {
    if (model != null) {
      model = null;
    }
  }

  @Override
  public void hotseatGameSelected() {
    model = new Reversi();
    view.showGame(model);
  }

  @Override
  public void aiGameSelected() {
    model = new AiGame(computer, aiDifficulty, settings);
    view.showGame(model);
  }

  @Override
  public void clientNetworkStartGameSelected(
      InetAddress serverAddress, Player player, String mode, String type, int id) {
    boolean gameIdAllowed = true;
    if (mode.matches("match") && type.matches("start")) {
      for (Pdu game : getOpenGames(serverAddress, mode)) {
        if (game.getGameID() == id) {
          gameIdAllowed = false;
        }
      }
    }
    if (gameIdAllowed) {
      client = new ReversiClient(player, id, mode);
      model = new NetworkGame(client);
      isNetworkGame = true;
      try {
        client.connect(serverAddress);
        client.syncState(model.getState(), type);
        view.showGame(model);
        receiverThread =
            new Thread(
                // receive as long a the phase is waiting
                // one last receive
                // expected if communication closes connection
                () -> {
                  try {
                    waitForGame();
                  } catch (Exception e) {
                    e.printStackTrace();
                  }
                });
        receiverThread.setDaemon(true);
        receiverThread.start();
      } catch (Exception e) {
        showStartView();
      }
    } else {
      view.showErrorMessage("GameId already in use!");
    }
  }

  @Override
  public Pdu[] getOpenGames(InetAddress serverAddress, String mode) {
    if (client == null) {
      client = new ReversiClient(Player.BLACK, 0, mode);
    }
    Pdu[] games = null;
    try {
      client.connect(serverAddress);
      games = client.getOpenGames();
    } catch (Exception e) {
      showStartView();
    }
    return games;
  }

  @Override
  public ReversiClient getClient() {
    return client;
  }

  @Override
  public void setClient(ReversiClient client) {
    this.client = client;
  }

  @Override
  public boolean isNetworkGame() {
    return isNetworkGame;
  }

  @Override
  public void networkViewSelected() {
    view.showNetworkView();
  }

  @Override
  public void settingsSelected() {
    view.showSettings();
  }

  @Override
  public void resetGame() throws IOException {
    model.newGame();
    if (client != null) {
      try {
        client.resetGame(model.getState());
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public boolean move(Cell to) {
    if (moveWorker != null) {
      showError("Move is already in progress");
      return false;
    }

    if (!GameField.isWithinBounds(to)) {
      showError("Target cell is out of bounds: " + to);
      return false;
    }

    // SwingWorker is designed to be executed only once. Executing it more often will
    // not result in invoking the doInBackground method for another time.
    // Due to this, we are using null references here in order to check if there are
    // any pending moves.
    moveWorker =
        new SwingWorker<>() {

          private void runReceiver() throws Exception {
            if (client != null) {
              try {
                GameState received = client.receive().getState();
                if (received.getMoveCounter() == 0) {
                  if (!client.getPlayer().equals(Player.BLACK)) {
                    received = client.receive().getState();
                  }
                }
                model.setState(received);
                view.showGame(model);
              } catch (Exception e) {
                throw new Exception(e);
              }
            }
          }

          @Override
          protected Boolean doInBackground() throws Exception {
            return model.move(to);
          }

          @Override
          protected void done() {
            moveWorker = null;
            try {
              if (!get()) {
                showError("Failed to execute the move");
              } else if (get() && client != null) {
                // waiting fo the next move in network game
                receiverThread =
                    new Thread(
                        () -> {
                          try {
                            runReceiver();
                          } catch (Exception e) {
                            e.printStackTrace();
                          }
                        });
                receiverThread.setDaemon(true);
                receiverThread.start();
              }
            } catch (InterruptedException e) {
              // We should not have to wait in the first place
              throw new IllegalStateException(e.getMessage(), e);
            } catch (ExecutionException e) {
              throw new RuntimeException(e.getMessage(), e);
            } catch (CancellationException e) {
              // moveWorker got interrupted, probably intentionally by the user.
              // --> do nothing here
            } catch (Exception e) {
              System.out.println("Could not set state to receive value");
            }
          }
        };

    moveWorker.execute();
    return true;
  }

  private void showError(String message) {
    view.showErrorMessage(message);
  }

  @Override
  public void dispose() {
    if (moveWorker != null) {
      // cancel the pending move
      moveWorker.cancel(true);
    }
  }

  private void waitForGame() {
    try {
      // receive as long a the phase is waiting
      while (model.getState().getCurrentPhase().equals(Phase.WAITING)) {
        GameState newState = client.receive().getState();
        model.setState(newState);
        view.showGame(model);
      }
      // one last receive
      GameState newState = client.receive().getState();
      model.setState(newState);
      view.showGame(model);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void setAiDifficulty(AiEngines ai) {
    this.aiDifficulty = ai;
  }

  @Override
  public void experimentalSettingsSelected() {
    view.showExperimentalSettings();
  }

  @Override
  public void updateInterConst1(int interConst1) {
    settings.setInterConst1(interConst1);
  }

  @Override
  public void updateInterConst2(int interConst2) {
    settings.setInterConst2(interConst2);
  }

  @Override
  public void updateInterConst3(int interConst3) {
    settings.setInterConst3(interConst3);
  }
}
