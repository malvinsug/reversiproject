package de.lmu.ifi.sosylab.reversi.model.ai;

import de.lmu.ifi.sosylab.reversi.model.Cell;
import de.lmu.ifi.sosylab.reversi.model.GameState;
import de.lmu.ifi.sosylab.reversi.model.Model;
import de.lmu.ifi.sosylab.reversi.model.Phase;
import de.lmu.ifi.sosylab.reversi.model.Player;
import java.util.Set;

/**
 * A heuristic that evaluates the board based on the disks on the field and on how many possible
 * moves are possible for each player.
 */
public class HeuristicSimple implements Heuristic {

  public static final int FACTOR = 4;
  private static final int WIN_FACTOR = 1048576;

  protected Model reversi;

  protected Player human;
  protected Player computer;

  /**
   * Constructor for the simple heuristic.
   *
   * @param reversi game model used for the evaluation
   * @param computer player who is played by the ai
   */
  HeuristicSimple(Model reversi, Player computer) {
    this.reversi = reversi;

    this.computer = computer;
    this.human = Player.getOpponentOf(computer);
  }

  /**
   * Computes the heuristic.
   *
   * @return the heuristic in int
   */
  public int heuristicEval() {
    Set<Cell> computerDisks = reversi.getState().getAllCellsOfPlayer(computer);
    Set<Cell> humanDisks = reversi.getState().getAllCellsOfPlayer(human);

    long easyEval = computerDisks.stream().count();
    easyEval = (2 * easyEval - FACTOR * humanDisks.stream().count());

    Set<Cell> computerMoveDisk = reversi.getPossibleMovesForDisk(computer);
    Set<Cell> humanMoveDisk = reversi.getPossibleMovesForDisk(human);

    easyEval = easyEval + 2 * computerMoveDisk.stream().count();
    easyEval = (easyEval - FACTOR * humanMoveDisk.stream().count());

    GameState state = reversi.getState();
    if (state.getCurrentPhase() == Phase.FINISHED) {
      if (state.getWinner().isPresent()) {
        Player winner = state.getWinner().get();
        int win = winner == computer ? WIN_FACTOR : -2 * WIN_FACTOR;
        easyEval = easyEval + win / (1 + state.getMoveCounter());
      }
    }

    return (int) easyEval;
  }

  @Override
  public void setModel(Model reversi) {
    this.reversi = reversi;
  }
}
