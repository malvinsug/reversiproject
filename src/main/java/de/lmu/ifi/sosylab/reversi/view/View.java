package de.lmu.ifi.sosylab.reversi.view;

import de.lmu.ifi.sosylab.reversi.model.Model;

/**
 * The main interface of the view. It gets the state it displays directly from the {@link Model}.
 */
public interface View {

  /** Show the graphical user interface of the Reversi game. */
  void showView();

  /** Shows the start menu to the user. */
  void showStartMenu();

  /** Shows the game view to the user. */
  void showGame(Model model);

  /** Shows the network lobby menu to the user. */
  void showNetworkView();

  /** Shows the setting menu to the user. */
  void showSettings();

  /** Removes a game-view if there is currently shown one. */
  void removeGame();

  /** Removes a settings menu if there is one currently shown. */
  void removeSettings();

  /** Removes a network lobby menu if there is one currently shown. */
  void removeNetworkView();

  /** Shows the experimental difficulty settings menu to the user. */
  void showExperimentalSettings();

  /** Removes the experimental settings menu if there is one currently shown. */
  void removeExperimentalSettings();

  /**
   * Displays an error message to the user.
   *
   * @param message The message to be displayed
   */
  void showErrorMessage(String message);
}
