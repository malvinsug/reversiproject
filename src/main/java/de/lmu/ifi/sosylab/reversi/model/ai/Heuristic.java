package de.lmu.ifi.sosylab.reversi.model.ai;

import de.lmu.ifi.sosylab.reversi.model.Model;

/** Interface for all heuristics. */
public interface Heuristic {

  /**
   * Method to evaluate the game based on the choosen heuristic.
   *
   * @return int value representing how high the moves scored on the heuristic
   */
  int heuristicEval();

  /**
   * Sets the model for the heuristic.
   *
   * <p>The model is needed to extract all needed information for the heuristic.
   *
   * @param reversi the concrete model
   */
  void setModel(Model reversi);
}
