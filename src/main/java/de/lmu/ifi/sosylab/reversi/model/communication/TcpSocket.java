package de.lmu.ifi.sosylab.reversi.model.communication;

import java.io.IOException;
import java.net.BindException;

/**
 * Implementation of the communication-Socket.
 *
 * @author mandlp
 */
public class TcpSocket {

  private java.net.ServerSocket serverSocket;
  private int sendBufferSize;
  private int receiveBufferSize;

  /**
   * Creates a TCP-communication-Socket binds it to a certain Port.
   *
   * @param port port number
   * @param sendBufferSize send-buffer size in byte
   * @param receiveBufferSize receive-buffer size in byte
   * @exception BindException Port already in use
   * @exception IOException I/O-Exception
   */
  public TcpSocket(int port, int sendBufferSize, int receiveBufferSize) throws Exception {

    this.sendBufferSize = sendBufferSize;
    this.receiveBufferSize = receiveBufferSize;
    try {
      serverSocket = new java.net.ServerSocket(port);
    } catch (Exception e) {
      throw e;
    }
  }

  public TcpConnection accept() throws IOException {
    return new TcpConnection(serverSocket.accept(), sendBufferSize, receiveBufferSize);
  }

  public void close() throws IOException {
    serverSocket.close();
  }

  public boolean isClosed() {
    return serverSocket.isClosed();
  }
}
