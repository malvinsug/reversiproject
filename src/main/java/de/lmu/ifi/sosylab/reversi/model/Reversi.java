package de.lmu.ifi.sosylab.reversi.model;

import static java.util.Objects.requireNonNull;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Implementation of a reversi class that handles the logic for a reversi game that consists of
 * Disks only. It is the entry-class to the internal structure of the game-model and provides all
 * methods necessary for an user-interface to playing a game successfully.
 */
public class Reversi implements Model {

  private static final int EXPECTED_HISTORY_LENGTH = 60;

  private final PropertyChangeSupport support = new PropertyChangeSupport(this);
  private final Deque<GameState> stateHistory = new ArrayDeque<>(EXPECTED_HISTORY_LENGTH);
  private GameState state;

  /**
   * Initialize a new Reversi-Game in which everything is set up in its initial position. The game
   * is ready to be played immediately after.
   */
  public Reversi() {
    newGame();
  }

  /**
   * Creates a new reversi object in which the passed {@link GameState}-object is taken as new
   * instance of this class.
   *
   * @param gameState A non-null {@link GameState}-object.
   */
  public Reversi(GameState gameState) {
    state = requireNonNull(gameState);
  }

  @Override
  public synchronized void addPropertyChangeListener(PropertyChangeListener pcl) {
    requireNonNull(pcl);
    support.addPropertyChangeListener(pcl);
  }

  @Override
  public synchronized void removePropertyChangeListener(PropertyChangeListener pcl) {
    requireNonNull(pcl);
    support.removePropertyChangeListener(pcl);
  }

  /**
   * Invokes the firing of an event, such that any attached observer (i.e., {@link
   * PropertyChangeListener}) is notified that a change happened to this model.
   */
  protected void notifyListeners() {
    notifyListeners(true);
  }

  private void notifyListeners(boolean wasActiveChange) {
    support.firePropertyChange(STATE_CHANGED, null, this);
    if (wasActiveChange) {
      support.firePropertyChange(NEW_MOVE, null, this);
    }
  }

  @Override
  public synchronized GameState getState() {
    return state;
  }

  /**
   * Sets the game state to the given value. Can be used to override the existing game state. After
   * updating the game state, listeners will be notified about the change.
   *
   * @param state the new game state
   */
  public synchronized void setState(GameState state) {
    this.state = state;
    notifyListeners(false);
  }

  @Override
  public synchronized void newGame() {
    state = new GameState();
    notifyListeners();
  }

  @Override
  public synchronized boolean move(Cell target) {
    if (state.getCurrentPhase() != Phase.RUNNING) {
      return false;
    }

    Player currentPlayer = state.getCurrentPlayer();
    // Source cell must be from the current active player
    if (!GameField.isWithinBounds(target) || state.getField().get(target).isPresent()) {
      return false;
    }

    // get all possible moves for the specific Disk
    Set<Cell> possibleMoves = getPossibleMovesForDisk(currentPlayer);
    if (!possibleMoves.contains(target)) {
      return false;
    }

    stateHistory.push(state.makeCopy());

    Set<Cell> isInLineCells = getInLineCells(target);
    state.getField().set(target, new Disk(state.getCurrentPlayer()));
    if (state.getMoveCounter() >= 4) {
      flipTheDisks(target, isInLineCells);
    }

    Player nextPlayer = Player.getOpponentOf(currentPlayer);

    GameState nextState = state.makeCopy();
    nextState.setCurrentPlayer(nextPlayer);
    nextState.increaseMoveCounter();
    Reversi nextGame = new Reversi(nextState);

    if (!nextGame.checkForWinningCondition(nextState)) {
      state.setCurrentPlayer(nextPlayer);
    } else {
      Optional<Player> winPlayer = findWinner(state);
      setGameFinished(winPlayer);
    }

    getState().increaseMoveCounter();
    notifyListeners();
    return true;
  }

  private Optional<Player> findWinner(GameState state) {
    int blackDisks = state.getAllCellsOfPlayer(Player.BLACK).size();
    int whiteDisks = state.getAllCellsOfPlayer(Player.WHITE).size();

    if (blackDisks > whiteDisks) {
      return Optional.of(Player.BLACK);

    } else if (blackDisks < whiteDisks) {

      return Optional.of(Player.WHITE);
    }
    return Optional.empty();
  }

  private void flipTheDisks(Cell target, Set<Cell> inLineCells) {
    verticalFlip(target, inLineCells);
    horizontalFlip(target, inLineCells);
    diagonalFlip(target, inLineCells);
  }

  private void verticalFlip(Cell target, Set<Cell> inLineCells) {
    int targetRow = target.getRow();
    Set<Cell> verticalCells =
        inLineCells.stream()
            .filter(c -> target.getColumn() == c.getColumn())
            .collect(Collectors.toSet());

    for (Cell cell : verticalCells) {
      int cellRow = cell.getRow();
      int rowDirection = (targetRow - cellRow) < 0 ? 1 : -1;
      Cell toFlipCell = Cell.createCell(target.getColumn(), target.getRow() + rowDirection);

      while (!toFlipCell.equals(cell)) {
        // Disk movedDisk = state.getField().remove(toFlipCell);
        state.getField().set(toFlipCell, new Disk(state.getCurrentPlayer()));
        toFlipCell = Cell.createCell(target.getColumn(), toFlipCell.getRow() + rowDirection);
      }
    }
  }

  private void horizontalFlip(Cell target, Set<Cell> inLineCells) {
    int targetColumn = target.getColumn();
    Set<Cell> horizontalCells =
        inLineCells.stream().filter(c -> target.getRow() == c.getRow()).collect(Collectors.toSet());
    for (Cell cell : horizontalCells) {
      int cellColumn = cell.getColumn();
      int columnDirection = (targetColumn - cellColumn) < 0 ? 1 : -1;
      Cell toFlipCell = Cell.createCell(target.getColumn() + columnDirection, target.getRow());

      while (!toFlipCell.equals(cell)) {
        state.getField().set(toFlipCell, new Disk(state.getCurrentPlayer()));
        toFlipCell = Cell.createCell(toFlipCell.getColumn() + columnDirection, target.getRow());
      }
    }
  }

  private void diagonalFlip(Cell target, Set<Cell> inLineCells) {
    int targetColumn = target.getColumn();
    int targetRow = target.getRow();
    Set<Cell> diagonalCells =
        inLineCells.stream()
            .filter(
                c ->
                    (Math.abs(target.getRow() - c.getRow())
                            - Math.abs(target.getColumn() - c.getColumn())
                        == 0))
            .collect(Collectors.toSet());

    for (Cell cell : diagonalCells) {
      int cellRow = cell.getRow();
      int cellColumn = cell.getColumn();

      int columnDirection = (targetColumn - cellColumn) < 0 ? 1 : -1;
      int rowDirection = (targetRow - cellRow) < 0 ? 1 : -1;

      Cell toFlipCell =
          Cell.createCell(target.getColumn() + columnDirection, target.getRow() + rowDirection);
      while (!toFlipCell.equals(cell)) {
        state.getField().set(toFlipCell, new Disk(state.getCurrentPlayer()));
        toFlipCell =
            Cell.createCell(
                toFlipCell.getColumn() + columnDirection, toFlipCell.getRow() + rowDirection);
      }
    }
  }

  @Override
  public synchronized void undoMove() {
    state = stateHistory.pop();
    notifyListeners();
  }

  /**
   * Checks whether a winning condition is fulfilled. In that case, the {@link Phase phase} and the
   * {@link Player winning player} are set appropriately.
   *
   * @return <code>true</code> if the game is over, <code>false</code> otherwise
   */
  private boolean checkForWinningCondition(GameState state) {
    // Check if all cells are occupied
    Set<Cell> emptyField = getEmptyFields();
    if (emptyField.isEmpty()) {
      return true;
    }
    if (state.getMoveCounter() > 3) {
      // Check if opponent still has possible moves
      Player opponentPlayer = Player.getOpponentOf(state.getCurrentPlayer());
      Set<Cell> possibleOpponentMoves = getPossibleMovesForDisk(opponentPlayer);

      if (possibleOpponentMoves.isEmpty()) {
        return true;
      }
    }

    return false;
  }

  /**
   * Ends the game by setting its current phase to {@link Phase#FINISHED}. The winner is also set
   * depending on the given input.
   *
   * <p>Afterwards, an event is fired in which all observers are notified about the changed phase.
   *
   * @param winner An {@link Optional} containing either the winning player, or empty otherwise (if
   *     the game ended in a draw).
   */
  private synchronized void setGameFinished(Optional<Player> winner) {
    state.setCurrentPhase(Phase.FINISHED);
    state.setWinner(winner);
    support.firePropertyChange(GAME_OVER, null, this);
    notifyListeners();
  }

  @Override
  public Set<Cell> getPossibleMovesForDisk(Player player) {
    if (player != Player.WHITE && player != Player.BLACK) {
      throw new IllegalArgumentException("Unhandled player: " + player);
    }

    Set<Cell> possibleMoves;
    if (state.getMoveCounter() < 4) {
      possibleMoves =
          getEmptyFields().stream().filter(this::getIsStartMove).collect(Collectors.toSet());
    } else {
      possibleMoves =
          getEmptyFields().stream().filter(this::getIsMovePossible).collect(Collectors.toSet());
    }

    return possibleMoves;
  }

  private boolean getIsMovePossible(Cell cell) {

    Set<Cell> emptyFields = getEmptyFields();
    boolean isEmptyGameCell = emptyFields.contains(cell);
    if (!isEmptyGameCell) {
      return false;
    }

    boolean isStartMove = getIsStartMove(cell);
    if (isStartMove) {
      return true;
    }

    boolean isConnected = isConnectedCell(cell);
    if (!isConnected) {
      return false;
    }

    return isInLine(cell);
  }

  private synchronized boolean isInLine(Cell target) {
    Set<Cell> inLineCells = getInLineCells(target);
    return !inLineCells.isEmpty();
  }

  private synchronized Set<Cell> getInLineCells(Cell target) {
    int targetColumn = target.getColumn();
    int targetRow = target.getRow();

    Set<Cell> occupiedCellsWithCurrentPlayer =
        state.getAllCellsOfPlayer(this.state.getCurrentPlayer());
    Set<Cell> inLineCells = new HashSet<>();

    for (Cell occupiedCell : occupiedCellsWithCurrentPlayer) {
      int sourceColumn = occupiedCell.getColumn();
      int sourceRow = occupiedCell.getRow();

      int colDiff = targetColumn - sourceColumn;
      int rowDiff = targetRow - sourceRow;
      int colRowDiff = Math.abs(colDiff) - Math.abs(rowDiff);

      boolean isHorizontal = rowDiff == 0;
      boolean isVertical = colDiff == 0;
      boolean isDiagonal = colRowDiff == 0;
      boolean isNotStraight = !(isHorizontal || isVertical || isDiagonal);
      if (Math.abs(colDiff) == 1 || Math.abs(rowDiff) == 1 || isNotStraight) {
        continue;
      }

      boolean isConnected =
          !gapExist(targetColumn, targetRow, sourceColumn, sourceRow, isHorizontal, isVertical);

      if (isConnected) {
        inLineCells.add(occupiedCell);
      }
    }

    return inLineCells;
  }

  private synchronized boolean gapExist(
      int targetColumn,
      int targetRow,
      int sourceColumn,
      int sourceRow,
      boolean isHorizontal,
      boolean isVertical) {

    int columnNotVertical = (targetColumn - sourceColumn) < 0 ? 1 : -1;
    int columnDirection = isVertical ? 0 : columnNotVertical;
    int rowNotHorizontal = (targetRow - sourceRow) < 0 ? 1 : -1;
    int rowDirection = isHorizontal ? 0 : rowNotHorizontal;

    boolean gapExist = false;

    int checkedColumn = targetColumn + columnDirection;
    int checkedRow = targetRow + rowDirection;

    boolean isChecking = (isVertical) ? checkedRow != sourceRow : checkedColumn != sourceColumn;

    while (isChecking) {
      Cell checkedCell = Cell.createCell(checkedColumn, checkedRow);
      Optional<Disk> optCell = this.state.getField().get(checkedCell);
      // if the cell is empty or is occupied by current player
      if (optCell.isEmpty() || optCell.get().getPlayer() == state.getCurrentPlayer()) {
        gapExist = true;
        break;
      }
      checkedColumn += columnDirection;
      checkedRow += rowDirection;

      isChecking = (isVertical) ? (checkedRow != sourceRow) : checkedColumn != sourceColumn;
    }
    return gapExist;
  }

  private synchronized boolean getIsStartMove(Cell cell) {
    boolean earlyGame = state.getMoveCounter() < 4;
    if (!earlyGame) {
      return false;
    }
    Set<Cell> startZone = new HashSet<>();

    startZone.add(Cell.createCell(3, 3));
    startZone.add(Cell.createCell(3, 4));

    startZone.add(Cell.createCell(4, 3));
    startZone.add(Cell.createCell(4, 4));

    return startZone.contains(cell);
  }

  private synchronized Set<Cell> getEmptyFields() {
    Set<Cell> allDisks = state.getAllCells();
    Set<Cell> emptyGameField = new HashSet<>();

    int size = GameField.SIZE;
    for (int col = 0; col < size; col++) {
      for (int row = 0; row < size; row++) {
        Cell cell = Cell.createCell(col, row);
        if (!allDisks.contains(cell)) {
          emptyGameField.add(cell);
        }
      }
    }

    return emptyGameField;
  }

  private synchronized boolean isConnectedCell(Cell cell) {
    int col = cell.getColumn();
    int row = cell.getRow();
    Player otherPlayer = Player.getOpponentOf(state.getCurrentPlayer());
    Set<Cell> disksOfOtherPlayer = state.getAllCellsOfPlayer(otherPlayer);
    Set<Cell> checkAgainstCells = new HashSet<>();

    checkAgainstCells.add(Cell.createCell(col + 1, row + 1));
    checkAgainstCells.add(Cell.createCell(col + 1, row + 0));
    checkAgainstCells.add(Cell.createCell(col + 1, row - 1));

    checkAgainstCells.add(Cell.createCell(col + 0, row + 1));
    checkAgainstCells.add(Cell.createCell(col + 0, row - 1));

    checkAgainstCells.add(Cell.createCell(col - 1, row + 1));
    checkAgainstCells.add(Cell.createCell(col - 1, row + 0));
    checkAgainstCells.add(Cell.createCell(col - 1, row - 1));

    checkAgainstCells.retainAll(disksOfOtherPlayer);

    return !checkAgainstCells.isEmpty();
  }
}
