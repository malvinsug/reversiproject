package de.lmu.ifi.sosylab.reversi.model;

/** ExperimentalAiSettings saves the interaction constant for RiskRegionsHeuristics. */
public class ExperimentalAiSettings {
  private int interConst1;
  private int interConst2;
  private int interConst3;

  /**
   * A constructor for saving the adjusted AI.
   *
   * @param interConst1 is interaction constant 1.
   * @param interConst2 is interaction constant 2.
   * @param interConst3 is interaction constant 3.
   */
  public ExperimentalAiSettings(int interConst1, int interConst2, int interConst3) {
    this.interConst1 = interConst1;
    this.interConst2 = interConst2;
    this.interConst3 = interConst3;
  }

  /**
   * returns the interaction constant 1.
   *
   * @return interaction constant 1
   */
  public int getInterConst1() {
    return interConst1;
  }

  /**
   * returns the interaction constant 2.
   *
   * @return interaction constant 2
   */
  public int getInterConst2() {
    return interConst2;
  }

  /**
   * returns the interaction constant 3.
   *
   * @return interaction constant 3
   */
  public int getInterConst3() {
    return interConst3;
  }

  /**
   * set the interaction constant 1.
   *
   * @param interConst1 the new value for interaction constant 1.
   */
  public void setInterConst1(int interConst1) {
    this.interConst1 = interConst1;
    System.out.println(this.interConst1);
  }

  /**
   * set the interaction constant 2.
   *
   * @param interConst2 the new value for interaction constant 2.
   */
  public void setInterConst2(int interConst2) {
    this.interConst2 = interConst2;
    System.out.println(this.interConst2);
  }

  /**
   * set the interaction constant 3.
   *
   * @param interConst3 the new value for interaction constant 3.
   */
  public void setInterConst3(int interConst3) {
    this.interConst3 = interConst3;
    System.out.println(this.interConst3);
  }
}
