package de.lmu.ifi.sosylab.reversi.view;

import static java.util.Objects.requireNonNull;

import de.lmu.ifi.sosylab.reversi.model.Model;
import de.lmu.ifi.sosylab.reversi.model.Phase;
import de.lmu.ifi.sosylab.reversi.model.Player;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Optional;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * The main view of the graphical user interface. It provides and connects all graphical elements
 * that are necessary for playing a reversi game. More specifically, it allows two players to
 * interactively execute their moves, and provides an adequate possibility for resetting the game at
 * any given time.
 */
class ReversiView extends JPanel implements PropertyChangeListener {

  private static final long serialVersionUID = 4230162211355981591L;

  private final DrawBoard drawBoard;

  private Model model;
  private Controller controller;

  private JPanel resetPanel;
  private JButton resetButton;
  private JButton quitGameButton;
  private JLabel infoLabel;
  private JLabel errorLabel;
  private JLabel scoreLabel;
  private JLabel stoneCountLabel;

  /**
   * Creates a view with all elements necessary for playing an interactive reversi game.
   *
   * @param model The {@link Model} that handles the logic of the game.
   * @param controller The {@link Controller} that validates and forwards any user input.
   */
  ReversiView(Model model, Controller controller) {
    this.model = requireNonNull(model);
    this.controller = requireNonNull(controller);

    drawBoard = new DrawBoard(model, controller);

    initializeWidgets();
    createView();
    updateToPhase();

    model.addPropertyChangeListener(this);
  }

  private void initializeWidgets() {
    int fontsize = 14;

    scoreLabel = new JLabel();
    scoreLabel.setForeground(Color.BLACK);
    scoreLabel.setFont(new Font(scoreLabel.getFont().getFontName(), Font.PLAIN, fontsize));
    scoreLabel.setAlignmentX(LEFT_ALIGNMENT);

    stoneCountLabel = new JLabel();
    stoneCountLabel.setForeground(Color.BLACK);
    stoneCountLabel.setFont(new Font(scoreLabel.getFont().getFontName(), Font.PLAIN, fontsize));
    stoneCountLabel.setAlignmentX(RIGHT_ALIGNMENT);

    resetPanel = new JPanel(new FlowLayout());

    infoLabel = new JLabel();
    infoLabel.setForeground(Color.RED);
    infoLabel.setFont(new Font(infoLabel.getFont().getFontName(), Font.PLAIN, fontsize));
    infoLabel.setHorizontalAlignment(JLabel.CENTER);
    infoLabel.setVerticalAlignment(JLabel.BOTTOM);

    quitGameButton = new JButton("Quit");
    quitGameButton.addActionListener(new ReversiViewListener());
    quitGameButton.setEnabled(true);
    resetButton = new JButton("Reset");
    resetButton.addActionListener(new ReversiViewListener());
    resetButton.setEnabled(true);
    errorLabel = new JLabel();
    errorLabel.setForeground(Color.RED);
    errorLabel.setFont(new Font(errorLabel.getFont().getFontName(), Font.PLAIN, fontsize));

    updatePlayerTextInDisplay();
    updatePlayerStoneCountInDisplay();
  }

  private void createView() {
    setLayout(new BorderLayout());
    JPanel panel = new JPanel(new FlowLayout());
    panel.add(stoneCountLabel);
    add(panel, BorderLayout.NORTH);

    JPanel gamePanel = new JPanel(new BorderLayout());
    gamePanel.add(infoLabel, BorderLayout.NORTH);
    gamePanel.add(drawBoard, BorderLayout.CENTER);
    add(gamePanel, BorderLayout.CENTER);

    resetPanel.add(quitGameButton);
    resetPanel.add(resetButton);
    resetPanel.add(errorLabel);

    add(resetPanel, BorderLayout.SOUTH);
  }

  private void updateToPhase() {
    if (model.getState().getCurrentPhase() == Phase.RUNNING) {
      drawBoard.setVisible(true);
    } else {
      drawBoard.setVisible(false);
      setInfoLabelText("Waiting for a new game to start");
    }
  }

  void showErrorMessage(String message) {
    errorLabel.setText(message);
  }

  /** Hides an error message that was previously displayed to the user. */
  private void hideErrorMessage() {
    errorLabel.setText("");
  }

  /** Updates the label text that informs the user about the current player. */
  private void updatePlayerTextInDisplay() {
    setInfoLabelText("Current player: " + model.getState().getCurrentPlayer());
  }

  /**
   * updates the stone counter that informs the user about the number of stones each user have on
   * board currently.
   */
  private void updatePlayerStoneCountInDisplay() {
    setStoneCountLabelText(
        "W: "
            + model.getState().getAllCellsOfPlayer(Player.WHITE).size()
            + "    "
            + "B: "
            + model.getState().getAllCellsOfPlayer(Player.BLACK).size());
  }

  void dispose() {
    model.removePropertyChangeListener(this);
    drawBoard.dispose();
  }

  @Override
  public void propertyChange(PropertyChangeEvent event) {
    SwingUtilities.invokeLater(() -> handleChangeEvent(event));
  }

  /**
   * The observable (= model) has just published that it has changed its state. The GUI needs to be
   * updated accordingly here.
   *
   * @param event The event that has been fired by the model.
   */
  private void handleChangeEvent(PropertyChangeEvent event) {
    if (event.getPropertyName().equals(Model.STATE_CHANGED)) {
      updateToPhase();
      updatePlayerTextInDisplay();
      updatePlayerStoneCountInDisplay();
      hideErrorMessage();
    }
    if (event.getPropertyName().equals(Model.GAME_OVER)) {

      openDialogIfGameIsOver();
    }
  }

  /**
   * Checks the model if the game has ended. In that case, a dialog is shown to the user in which a
   * respective message with the winner is shown.
   */
  private void openDialogIfGameIsOver() {
    if (model.getState().getCurrentPhase() != Phase.FINISHED) {
      return;
    }

    Optional<Player> playerOpt = model.getState().getWinner();
    if (playerOpt.isPresent()) {
      Player p = playerOpt.get();
      setInfoLabelText("Player " + p + " has won. Click reset to start a new game.");
      showDialogWindow("Finished!", "There's a winner: Player " + p);
    } else {
      setInfoLabelText("Game ended as a tie. Click reset to start a new game.");
      showDialogWindow("Finished!", "Game is over. It's a tie!");
    }
  }

  /**
   * Shows a message pane that displays the outcome of the game.
   *
   * @param header The header text.
   * @param message An elaborate message why the game has ended.
   */
  private void showDialogWindow(String header, String message) {
    Optional<ImageIcon> iconOpt = ResourceLoader.loadScaledImageIcon("/award.png", 60, 80);

    JOptionPane.showMessageDialog(
        this,
        message,
        header,
        JOptionPane.INFORMATION_MESSAGE,
        iconOpt.isPresent() ? iconOpt.get() : null);
  }

  /**
   * Display a message in the view.
   *
   * @param message The message to be displayed (covers Infolabel , ScoreLabel , StoneCountLabel)
   */
  private void setInfoLabelText(String message) {
    infoLabel.setText(message);
  }

  private void setStoneCountLabelText(String message) {
    stoneCountLabel.setText(message);
  }

  /**
   * A custom implementation of an {@link ActionListener} that is used for observing and forwarding
   * any actions made on the respective widgets of the {@link ReversiView}.
   */
  private class ReversiViewListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      if (e.getSource() == resetButton) {
        handleResetButtonClick();
      } else if (e.getSource() == quitGameButton) {
        handleQuitGameButtonClick();
      }
    }

    private void handleResetButtonClick() {
      int result =
          JOptionPane.showConfirmDialog(
              ReversiView.this,
              "Do you really want to start a new game?",
              "Please confirm your choice",
              JOptionPane.YES_NO_OPTION,
              JOptionPane.QUESTION_MESSAGE);

      // do nothing if user didn't click on the 'yes'-option
      if (result == JOptionPane.YES_OPTION) {
        try {
          controller.resetGame();
        } catch (IOException e) {
          throw new AssertionError("Can't handle IOException in a meaningful way", e);
        }
      }
    }

    private void handleQuitGameButtonClick() {
      int result =
          JOptionPane.showConfirmDialog(
              ReversiView.this,
              "Do you really want to abort this game?",
              "Please confirm your choice",
              JOptionPane.YES_NO_OPTION,
              JOptionPane.QUESTION_MESSAGE);

      // do nothing if user didn't click on the 'yes'-option
      if (result == JOptionPane.YES_OPTION) {
        if (controller.isNetworkGame()) {
          try {
            controller.getClient().removeGame();
            controller.setClient(null);
          } catch (Exception e) {
            System.out.println("There was an error closing the connnection to the communication.");
          }
        }
        controller.showStartView();
      }
    }
  }
}
