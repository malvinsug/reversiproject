package de.lmu.ifi.sosylab.reversi.view;

import static java.util.Objects.requireNonNull;

import de.lmu.ifi.sosylab.reversi.model.AiGame;
import de.lmu.ifi.sosylab.reversi.model.Cell;
import de.lmu.ifi.sosylab.reversi.model.Disk;
import de.lmu.ifi.sosylab.reversi.model.GameField;
import de.lmu.ifi.sosylab.reversi.model.Model;
import de.lmu.ifi.sosylab.reversi.model.Phase;
import de.lmu.ifi.sosylab.reversi.model.Player;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * A custom painting class that is responsible for drawing the complete reversi board along with all
 * necessary informations on it. This includes a proper display of each single reversi field as well
 * as the available Disks in the game. Besides, all possible moves for a single Disk are painted,
 * should a user click on one.
 */
class DrawBoard extends JPanel implements PropertyChangeListener {

  private static final long serialVersionUID = 917034566486380490L;

  private final Model model;
  private final Controller controller;

  private Set<Cell> possibleMovesForSelectedCell = new HashSet<>();

  private int cellSize;

  private int horizontalOffset;
  private int verticalOffset;
  private static final Color REVERSI_GREEN = new Color(0, 153, 76);

  /**
   * Creates a drawboard used to draw the reversi field. It requires a model for being able to
   * retrieve the Disks and their possible moves, and a controller for forwarding the
   * user-interactions done on this sboard.
   *
   * @param model The model for getting the necessary informations about the Disks.
   * @param controller A controller to forward the interactions of the user.
   */
  DrawBoard(Model model, Controller controller) {
    this.model = requireNonNull(model);
    this.controller = requireNonNull(controller);

    model.addPropertyChangeListener(this);

    setBackground(new Color(110, 110, 110));
    configureActionListener();
  }

  private void configureActionListener() {
    addComponentListener(
        new ComponentAdapter() {

          @Override
          public void componentResized(ComponentEvent e) {
            repaint();
          }
        });

    addMouseListener(new DrawboardListener());
  }

  /**
   * Removes all move hints of the Disks and repaints the board in case they were previously shown.
   */
  private void removeMovingHints() {
    if (!possibleMovesForSelectedCell.isEmpty()) {
      possibleMovesForSelectedCell.clear();
      repaint();
    }
  }

  @Override
  protected void paintComponent(Graphics g) {

    Graphics2D g2D = (Graphics2D) g;
    // optional rendering options that are used for preference when drawing the image-icons
    g2D.setRenderingHint(
        RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);

    // paint the background
    g2D.setColor(Color.LIGHT_GRAY);
    g2D.fillRect(0, 0, getWidth(), getHeight());

    // gap-size between each cell and its actual border
    int padding = 5;

    // the size of a single cell including its border
    // (simply resize the reversi-window for a demo)
    cellSize =
        Math.min(
            ((getWidth() - padding) * 14 / 15 / GameField.SIZE),
            ((getHeight() - padding) * 9 / 10 / GameField.SIZE));

    // remove the right/bottom border of all cells
    int cellWidth = cellSize - padding;
    int cellHeight = cellWidth;

    // compute the offset, i.e. the value at which the first cell is actually drawn
    horizontalOffset = (getWidth() - cellSize * GameField.SIZE + padding) / 2;
    verticalOffset = (getHeight() - cellSize * GameField.SIZE + padding) / 2;

    // paint the background of the actual field blue
    g2D.setColor(Color.DARK_GRAY);
    g2D.fillRect(
        horizontalOffset - padding,
        verticalOffset - padding,
        getWidth() - 2 * (horizontalOffset - padding),
        getHeight() - 2 * (verticalOffset - padding));

    // column and row value ranging from 0 to 7
    int col = 0;
    int row = 0;

    int idx = horizontalOffset;
    int idy = verticalOffset;

    // flag used to decide the foreground-color of a cell (i.e., light or dark)
    boolean evenColumn = true;

    possibleMovesForSelectedCell =
        model.getPossibleMovesForDisk(model.getState().getCurrentPlayer());

    while (col < GameField.SIZE) {
      row = 0;
      idy = verticalOffset;
      boolean drawCell = evenColumn;
      while (row < GameField.SIZE) {

        // draw the background of each cell, which is a custom green color fitted for Reversi game
        if (true) {
          g2D.setColor(REVERSI_GREEN);
        }
        g2D.drawRect(idx, idy, cellWidth, cellHeight);
        g2D.fillRect(idx, idy, cellWidth, cellHeight);

        Cell cell = Cell.createCell(col, GameField.SIZE - row - 1);

        // highlight this cell if a Disk is currently selected and this field is a possible
        // target cell
        if (!(model instanceof AiGame)) {
          if (possibleMovesForSelectedCell.contains(cell)) {
            if (model.getState().getCurrentPhase() != Phase.FINISHED) {
              g2D.setColor(Color.RED);
              highlightCell(g2D, idx, idy, cellWidth, cellHeight);
            }
          }
        } else {
          AiGame aiGame = (AiGame) model;
          Player human = Player.getOpponentOf(aiGame.getComputerRole());
          if (model.getState().getCurrentPlayer() == human
              && possibleMovesForSelectedCell.contains(cell)) {
            if (model.getState().getCurrentPhase() != Phase.FINISHED) {
              g2D.setColor(Color.RED);
              highlightCell(g2D, idx, idy, cellWidth, cellHeight);
            }
          }
        }

        // draw a Disk on the cell if its owned by a player
        Optional<Player> playerOpt = model.getState().getField().get(cell).map(Disk::getPlayer);
        if (playerOpt.isPresent()) {
          drawDisk(playerOpt.get(), g2D, padding, idx, idy, cellWidth, cellHeight);
        }

        drawCell = !drawCell;
        idy = idy + cellHeight + padding;
        row++;
      }

      evenColumn = !evenColumn;
      idx = idx + cellWidth + padding;
      col++;
    }
  }

  /**
   * Draw a Disk on the current selected cell.
   *
   * @param player The player that owns the cell.
   * @param g The {@link Graphics} object that allows to draw on the board.
   * @param padding Used to determine the gap-size between the cell and its border
   * @param x The coordinate marking the left point of the cell.
   * @param y The coordinate marking the upper point of the cell.
   * @param cellWidth The width of the cell.
   * @param cellHeight The height of the cell.
   */
  private void drawDisk(
      Player player, Graphics g, int padding, int x, int y, int cellWidth, int cellHeight) {
    Optional<Image> imgOpt = Optional.empty();

    switch (player) {
      case WHITE:
        g.setColor(Color.WHITE);
        imgOpt = ResourceLoader.WHITE_DISK;
        break;
      case BLACK:
        g.setColor(Color.BLACK);
        imgOpt = ResourceLoader.BLACK_DISK;
        break;
      default:
        throw new RuntimeException("Unhandled player: " + player);
    }

    if (imgOpt.isPresent()) {
      g.drawImage(
          imgOpt.get(),
          x + padding,
          y + padding,
          cellWidth - 2 * padding,
          cellHeight - 2 * padding,
          null);
    } else {
      // draw circles as fallback when loading the images failed
      g.fillOval(x + padding, y + padding, cellWidth - 2 * padding, cellHeight - 2 * padding);

      g.setColor(Color.BLACK);
      g.drawOval(x + padding, y + padding, cellWidth - 2 * padding, cellHeight - 2 * padding);
    }
  }

  /**
   * Draw all available moves for the current selected cell.
   *
   * @param g2d The {@link Graphics2D} object that allows to draw on the board.
   * @param x The coordinate marking the left point of the cell.
   * @param y The coordinate marking the upper point of the cell.
   * @param cellWidth The width of the cell.
   * @param cellHeight The height of the cell.
   */
  private void highlightCell(Graphics2D g2d, int x, int y, int cellWidth, int cellHeight) {
    Stroke defaultStroke = g2d.getStroke();

    // Graphics#drawRect draws only the border of the rectangle. The stroke-object is used
    // afterwards to adjust the thickness of the lines.
    g2d.setStroke(new BasicStroke(3));
    g2d.drawRect(x, y, cellWidth, cellHeight);

    // Set the stroke back to its original value, otherwise all further cell-lines would be drawn
    // with the same new thickness.
    g2d.setStroke(defaultStroke);
  }

  /**
   * Check if the registered mouse-click event was made on the painted cells of the drawing board.
   *
   * <p>In this case an {@link Optional} with the actual {@link Cell} is returned.
   *
   * @param p the point of the registered mouse click event.
   * @return An {@link Optional} containing the x- and y-coordinate of the cell.
   */
  private Optional<Cell> screenToBoardCoords(Point p) {
    // check if registered click is on one of the drawn cells of the board
    if (p.x <= horizontalOffset
        || p.y <= verticalOffset
        || p.x >= getWidth() - horizontalOffset
        || p.y >= getHeight() - verticalOffset
        || cellSize == 0) {
      return Optional.empty();
    }

    // turn point to the actual cell coordinate
    int column = (p.x - horizontalOffset) / cellSize;
    int row = (p.y - verticalOffset) / cellSize;

    return Optional.of(new Cell(column, GameField.SIZE - row - 1));
  }

  @Override
  public void propertyChange(PropertyChangeEvent event) {
    SwingUtilities.invokeLater(() -> handlePropertyChange(event));
  }

  /**
   * The model has just announced that it has changed its state. The method will thus repaint the
   * board such that the display will show again the latest state.
   *
   * @param event The event that has been fired by the model.
   */
  private void handlePropertyChange(PropertyChangeEvent event) {
    if (event.getPropertyName().equals(Model.STATE_CHANGED)) {
      repaint();
    }
  }

  /** Disposes the drawboard by unsubscribing it from the model. */
  void dispose() {
    model.removePropertyChangeListener(this);
  }

  /**
   * A controller class for the {@link DrawBoard}. It overwrites the {@link
   * java.awt.event.MouseListener}-interface so that any clicks made by the user on the reversi
   * board are registered and handled accordingly here.
   */
  class DrawboardListener extends MouseAdapter {

    // Optional containing the last clicked cell if the Disk on it belongs to the user
    // private Optional<Cell> sourceCellOpt = Optional.empty();

    @Override
    public void mouseClicked(MouseEvent e) {
      if (model.getState().getCurrentPhase() == Phase.FINISHED) {
        disableMoveHints();
        return;
      }

      if (model instanceof AiGame && model.getState().getCurrentPlayer() == Player.WHITE) {
        disableMoveHints();
        return;
      }

      if (e.getButton() == MouseEvent.BUTTON2) {
        // user used middle-click --> do nothing
        return;
      }

      if (e.getButton() == MouseEvent.BUTTON3) {
        // user used right-click --> clear all possible moves if there are any
        disableMoveHints();
        return;
      }

      Optional<Cell> cellOpt = screenToBoardCoords(e.getPoint());
      if (cellOpt.isEmpty()) {
        // user clicked on the light-gray area outside of the blue background
        disableMoveHints();
        return;
      }

      if (possibleMovesForSelectedCell.contains(cellOpt.get())) {
        controller.move(cellOpt.get());
      }
      disableMoveHints();
    }

    private void disableMoveHints() {
      removeMovingHints();
    }
  }
}
