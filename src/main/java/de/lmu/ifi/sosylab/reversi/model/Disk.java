package de.lmu.ifi.sosylab.reversi.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/** A class representing a single pawn of the reversi board including its corresponding color. */
public class Disk implements Serializable {

  private static final long serialVersionUID = -8277849825355245303L;

  @JsonProperty("disk")
  private Player player;

  /**
   * Create a new <code>Disk</code>-object that is owned by the specified player.
   *
   * @param player The owner of the disk.
   */
  @JsonCreator
  Disk(@JsonProperty("player") Player player) {
    this.player = player;
  }

  /** flips the player of the disk. */
  void flipPlayer() {
    this.player = this.player == Player.WHITE ? Player.BLACK : Player.WHITE;
  }

  /**
   * Return the {@link Player} that is the owner of this <code>Disk</code>.
   *
   * @return The owning player.
   */
  @JsonIgnore
  public Player getPlayer() {
    return player;
  }
}
