package de.lmu.ifi.sosylab.reversi.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Utility class for the reversi game with usefull function.
 *
 * <p>It is not officially supported and is just useful for debuffing
 */
public class ReversiUtility {

  public static final Player STARTING_PLAYER = Player.BLACK;
  public static final char STARTING_CHAR = 'A';

  /**
   * This method is there to convert a input string to a list of cells.
   *
   * <p>The method assumes the input data is correct and that the input is in the format
   * move1,move2,move3 - e.g. c1,c2,c4... moves and simple location of disks have the same format so
   * this method does cover both.
   *
   * @param moves is the string for the moves
   * @return List of cells
   */
  public static List<Cell> stringOfMovesToGameMoves(String moves) {
    String[] targetTokeStrings = moves.trim().toUpperCase().split(",");
    List<Cell> targets = new ArrayList<>();
    for (String move : targetTokeStrings) {
      targets.add(convertStringToCell(move));
    }
    return targets;
  }

  private static Cell convertStringToCell(String move) {
    if (move.length() != 2) {
      throw new IllegalArgumentException("move can only have a string of length 2");
    }
    int first = (int) move.charAt(0) - (int) STARTING_CHAR;
    int second = Character.getNumericValue(move.charAt(1)) - 1;

    return new Cell(first, second);
  }

  /**
   * This methods creates a String of a list of inputs cells.
   *
   * <p>It does not check if the input is valid. The output will be in the format cell1,cell2,cell3
   *
   * @param targetDisks are all Cells who should be returned as string
   * @return cells converted to String
   */
  public static String convertCellsToString(List<Cell> targetDisks) {

    StringBuilder stringOfDisks = new StringBuilder();
    for (Cell disks : targetDisks) {
      stringOfDisks.append(convertCellToString(disks));
    }

    return stringOfDisks.toString();
  }

  private static String convertCellToString(Cell disks) {
    Character first = (char) (disks.getColumn() + (int) STARTING_CHAR);
    int second = disks.getRow() + 1;
    return first.toString() + second;
  }

  /**
   * Creates a GameState from the list of disks of black and white disks.
   *
   * @param blackDisks list of black disks
   * @param whiteDisks list of white disks
   * @return
   */
  public static GameState createGameState(List<Cell> blackDisks, List<Cell> whiteDisks) {
    GameState state = new GameState();
    for (Cell cell : whiteDisks) {
      state.getField().set(cell, new Disk(Player.WHITE));
    }
    for (Cell cell : blackDisks) {
      state.getField().set(cell, new Disk(Player.BLACK));
    }
    return state;
  }

  /**
   * Creates a full GameState of from the input.
   *
   * @param blackDisks list of black disks
   * @param whiteDisks list of white disks
   * @param currentPlayer whos current turn is
   * @param movecounter the given move counter
   * @param currentPhase is the current phase
   * @param winner is the winner of the game
   * @return gamestate created from input
   */
  public static GameState createGameState(
      List<Cell> blackDisks,
      List<Cell> whiteDisks,
      Player currentPlayer,
      int movecounter,
      Phase currentPhase,
      Optional<Player> winner) {
    GameState state = createGameState(blackDisks, whiteDisks);
    state.setCurrentPhase(currentPhase);
    state.setCurrentPlayer(currentPlayer);
    state.setWinner(winner);
    for (int i = 0; i < movecounter; i++) {
      state.increaseMoveCounter();
    }
    return state;
  }

  /**
   * Generates from a string of numbers an array of cells.
   *
   * <p>It will convert "2,3"->Cell(2,3). This method does not check if the input is valid
   *
   * @param numString the string to convert
   * @return array of cells
   */
  public static List<Cell> convertNumStringToCells(String numString) {
    String[] tokenCellStrings = numString.trim().split(",");
    List<Cell> disksCells = new ArrayList<>();
    for (int i = 0; i < tokenCellStrings.length; i = i + 2) {
      int col = Integer.parseInt(tokenCellStrings[i]);
      int row = Integer.parseInt(tokenCellStrings[i + 1]);
      disksCells.add(new Cell(col, row));
    }
    return disksCells;
  }
}
