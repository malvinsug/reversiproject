package de.lmu.ifi.sosylab.reversi.model;

import de.lmu.ifi.sosylab.reversi.model.communication.Pdu;
import de.lmu.ifi.sosylab.reversi.model.communication.ReversiClient;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/** NetworkGame. */
public class NetworkGame2 implements Model, PropertyChangeListener {

  private ReversiClient client;
  private Reversi delegate = new Reversi();
  private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

  /**
   * Create a new network Game (for client).
   *
   * @param client The client that started the game
   */
  public NetworkGame2(ReversiClient client) {
    this.client = client;
    delegate.getState().setCurrentPhase(Phase.WAITING);
  }

  @Override
  public void addPropertyChangeListener(PropertyChangeListener pcl) {
    delegate.addPropertyChangeListener(pcl);
  }

  @Override
  public void removePropertyChangeListener(PropertyChangeListener pcl) {
    delegate.removePropertyChangeListener(pcl);
  }

  @Override
  public void newGame() throws IOException {
    delegate.newGame();
    getState().setCurrentPhase(Phase.WAITING);
  }

  @Override
  public void setState(GameState state) {
    delegate.setState(state);
  }

  @Override
  public Set<Cell> getPossibleMovesForDisk(Player player) {
    if (delegate.getState().getCurrentPlayer() == client.getPlayer()) {
      return delegate.getPossibleMovesForDisk(player);
    } else {
      return new HashSet<Cell>();
    }
  }

  @Override
  public synchronized boolean move(Cell target) {
    GameState currentState = getState();
    if (client.getPlayer().equals(currentState.getCurrentPlayer())) {
      if (!delegate.move(target)) {
        return false;
      } else {
        try {
          Pdu newPdu = client.checkMove(getState());
          GameState newState = newPdu.getState();
          if (newState != null) {
            setState(newState);
          } else {
            System.out.println("The communication is not answering properly.");
          }
          return true;
        } catch (Exception e) {
          System.out.println("Exception while checking the Move");
          return false;
        }
      }
    } else {
      System.out.println("Its not your turn");
      return false;
    }
  }

  void subscribe(PropertyChangeListener pcl) {
    pcs.addPropertyChangeListener(pcl);
  }

  @Override
  public void undoMove() {
    delegate.undoMove();
  }

  @Override
  public GameState getState() {
    return delegate.getState();
  }

  @Override
  public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
    if (client.secondPlayer && getState().getCurrentPhase() == Phase.WAITING) {
      getState().setCurrentPhase(Phase.RUNNING);
      delegate.notifyListeners();
    }

    if (!client.connected) {
      getState().setCurrentPhase(Phase.FINISHED);
      delegate.notifyListeners();
    }
  }
}
