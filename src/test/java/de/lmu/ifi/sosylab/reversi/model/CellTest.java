package de.lmu.ifi.sosylab.reversi.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

@TestInstance(Lifecycle.PER_CLASS)
public class CellTest {

  @BeforeAll
  void setUp() {}

  @AfterAll
  void tearDown() {}

  @Test
  void testAllCells() {
    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 8; j++) {
        correctValues(i, j);
        equalCells(i, j);
      }
    }
  }

  void correctValues(int col, int row) {

    Cell cell = new Cell(col, row);

    int actualCol = cell.getColumn();
    int actualRow = cell.getRow();

    int expectedCol = col;
    int expectedRow = row;

    assertEquals(expectedCol, actualCol);
    assertEquals(expectedRow, actualRow);
  }

  void equalCells(int i, int j) {
    Cell cell1 = new Cell(i, j);
    assertEquals(cell1.equals(cell1), true);
    assertEquals((Cell.createCell(i, j)).equals(cell1), true);
    Cell cell2 = new Cell(i, j);
    assertEquals(cell1.equals(cell2), true);
    Cell cell3 = new Cell(i, j + i + 1);
    assertEquals(cell1.equals(cell3), false);
    assertEquals((Cell.createCell(i, j)).equals(cell3), false);
    Cell cell4 = new Cell(i + 1 + j, j);
    assertFalse(cell1.equals(cell4));
    assertFalse(cell1.equals(Player.BLACK));
  }

  @Test
  void testHashIsImplemented() {
    Cell cell1 = new Cell(1, 1);
    Cell cell2 = new Cell(2, 1);
    Cell cell3 = new Cell(1, 1);
    assertEquals(cell1.hashCode(), cell3.hashCode());
    assertNotEquals(cell2.hashCode(), cell3.hashCode());
  }

  @ParameterizedTest
  @CsvSource(value = {"1,2", "2,3", "4,4", "9,9"})
  void toStringTest(int col, int row) {
    Cell cell = new Cell(col, row);
    assertEquals(cell.toString(), col + "," + row);
  }
}
