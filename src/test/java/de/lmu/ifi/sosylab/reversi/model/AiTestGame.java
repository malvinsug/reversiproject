package de.lmu.ifi.sosylab.reversi.model;

import static org.junit.jupiter.api.Assertions.assertTrue;

import de.lmu.ifi.sosylab.reversi.model.ai.AiEngines;
import java.util.Set;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class AiTestGame {

  @BeforeAll
  void setUp() {}

  @AfterAll
  void tearDown() {}

  @Test
  void testEasyAI() {
    Model game = new AiGame(Player.WHITE, AiEngines.EASY_AI);
    Set<Cell> moves = game.getPossibleMovesForDisk(Player.BLACK);
    for (int i = 0; i < 8; i++) {
      moves = game.getPossibleMovesForDisk(Player.BLACK);
      Cell target = moves.stream().findAny().get();
      boolean movePossible = game.move(target);
      assertTrue(movePossible);
    }
  }

  @Test
  void testSimpleMiniMax() {
    Model game = new AiGame(Player.WHITE, AiEngines.MINIMAX_SIMPLE_HEURISTIC);
    Set<Cell> moves = game.getPossibleMovesForDisk(Player.BLACK);
    for (int i = 0; i < 8; i++) {
      moves = game.getPossibleMovesForDisk(Player.BLACK);
      Cell target = moves.stream().findAny().get();
      boolean movePossible = game.move(target);
      assertTrue(movePossible);
    }
  }

  @Test
  void testRiskMiniMax() {
    Model game = new AiGame(Player.WHITE, AiEngines.MINIMAX_ADVANCED_HEURISTIC);
    Set<Cell> moves = game.getPossibleMovesForDisk(Player.BLACK);
    for (int i = 0; i < 8; i++) {
      moves = game.getPossibleMovesForDisk(Player.BLACK);
      Cell target = moves.stream().findAny().get();
      boolean movePossible = game.move(target);
      assertTrue(movePossible);
    }
  }
}
