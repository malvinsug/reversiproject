package de.lmu.ifi.sosylab.reversi.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class GameLogicTest {

  Model reversi;

  @BeforeEach
  void setUp() {
    this.reversi = new Reversi();
  }

  @AfterAll
  void tearDown() {}

  @Test
  void testFullGame() {
    List<Cell> moves = new ArrayList<>();
    moves.add(new Cell(3, 5));
    moves.add(new Cell(2, 4));
    moves.add(new Cell(4, 5));
    moves.add(new Cell(5, 4));
    moves.add(new Cell(1, 3));
    moves.add(new Cell(5, 2));
    moves.add(new Cell(4, 2));
    moves.add(new Cell(3, 6));
    moves.add(new Cell(2, 5));
    moves.add(new Cell(3, 2));
    moves.add(new Cell(5, 5));
    moves.add(new Cell(6, 6));

    List<Cell> movesEarlyGame = new ArrayList<>();
    movesEarlyGame.add(new Cell(3, 3));
    movesEarlyGame.add(new Cell(3, 4));
    movesEarlyGame.add(new Cell(4, 3));
    movesEarlyGame.add(new Cell(4, 4));

    List<Cell> wrongMoves = new ArrayList<>();
    wrongMoves.add(new Cell(0, 0));

    earlyGameMoves(movesEarlyGame);
    // game need NOT to be finished and last move should be tested separately
    makeAPossibleMoves(moves);

    wrongMoves(wrongMoves);

    testUndoMove();
  }

  void makeAPossibleMoves(List<Cell> moves) {
    for (Cell cell : moves) {
      Player currentPlayerBeforeMove = reversi.getState().getCurrentPlayer();
      // move is in the set of possible moves
      assertTrue(reversi.getPossibleMovesForDisk(currentPlayerBeforeMove).contains(cell));
      // can do move
      assertTrue(reversi.move(cell));
      // new cell is in getAllCells
      assertTrue(reversi.getState().getAllCellsOfPlayer(currentPlayerBeforeMove).contains(cell));
      assertTrue(reversi.getState().getAllCells().contains(cell));
      // phase is running
      assertEquals(Phase.RUNNING, reversi.getState().getCurrentPhase());
      // the players did switch
      assertNotEquals(currentPlayerBeforeMove, reversi.getState().getCurrentPlayer());

      assertTrue(reversi.getState().getField().isCellOfPlayer(currentPlayerBeforeMove, cell));
    }
  }

  void earlyGameMoves(List<Cell> movesEarlyGame) {
    for (Cell cell : movesEarlyGame) {
      Player currentPlayerBeforeMove = reversi.getState().getCurrentPlayer();
      // move is in the set of possible moves
      assertTrue(reversi.getPossibleMovesForDisk(currentPlayerBeforeMove).contains(cell));
      // can do move
      assertTrue(reversi.move(cell));
      // new cell is in getAllCells
      assertTrue(reversi.getState().getAllCellsOfPlayer(currentPlayerBeforeMove).contains(cell));
      assertTrue(reversi.getState().getAllCells().contains(cell));
      // phase is running
      assertEquals(Phase.RUNNING, reversi.getState().getCurrentPhase());
      // the players did switch
      assertNotEquals(currentPlayerBeforeMove, reversi.getState().getCurrentPlayer());

      assertTrue(reversi.getState().getField().isCellOfPlayer(currentPlayerBeforeMove, cell));
    }
  }

  void wrongMoves(List<Cell> moves) {
    for (Cell cell : moves) {
      Player currentPlayerBeforeMove = reversi.getState().getCurrentPlayer();
      // move is not in the set of possible moves
      assertFalse(reversi.getPossibleMovesForDisk(currentPlayerBeforeMove).contains(cell));
      // can't do move
      assertFalse(reversi.move(cell));
      // new cell is not in getAllCells
      assertFalse(reversi.getState().getAllCellsOfPlayer(currentPlayerBeforeMove).contains(cell));
      assertFalse(reversi.getState().getAllCells().contains(cell));
      // phase is running
      assertEquals(Phase.RUNNING, reversi.getState().getCurrentPhase());
      // the players did not switch
      assertEquals(currentPlayerBeforeMove, reversi.getState().getCurrentPlayer());

      assertFalse(reversi.getState().getField().isCellOfPlayer(currentPlayerBeforeMove, cell));
    }
  }

  void testUndoMove() {
    Cell lastMove = new Cell(6, 6);
    reversi.undoMove();
    assertFalse(
        reversi
            .getState()
            .getAllCellsOfPlayer(reversi.getState().getCurrentPlayer())
            .contains(lastMove));
    assertFalse(
        reversi
            .getState()
            .getField()
            .isCellOfPlayer(reversi.getState().getCurrentPlayer(), lastMove));
  }

  @Test
  void testStartConfig() {
    Phase actualPhase = reversi.getState().getCurrentPhase();
    assertEquals(Phase.RUNNING, actualPhase);
    Player actualPlayer = reversi.getState().getCurrentPlayer();
    assertEquals(Player.BLACK, actualPlayer);
    assertEquals(0, reversi.getState().getMoveCounter());
    assertTrue(reversi.getState().getAllCells().isEmpty());
    assertTrue(reversi.getState().getAllCellsOfPlayer(Player.BLACK).isEmpty());
    assertTrue(reversi.getState().getAllCellsOfPlayer(Player.WHITE).isEmpty());
  }

  @Test
  void phaseTest() {
    reversi.getState().setCurrentPhase(Phase.FINISHED);
    assertFalse(reversi.move(new Cell(3, 3)));
  }

  @Test
  void isWithinBoundsTest() {
    assertFalse(reversi.move(new Cell(100, 100)));
    reversi.move(new Cell(3, 3));
    assertFalse(reversi.move(new Cell(3, 3)));
  }

  @Test
  void finishedGameTest() {
    reversi.move(new Cell(3, 3));
    reversi.move(new Cell(3, 4));
    reversi.move(new Cell(4, 4));
    reversi.move(new Cell(4, 3));

    reversi.move(new Cell(3, 5));
    reversi.move(new Cell(2, 5));
    reversi.move(new Cell(1, 5));
    reversi.move(new Cell(3, 6));
    reversi.move(new Cell(4, 7));
    reversi.move(new Cell(3, 2));
    reversi.move(new Cell(3, 1));
    reversi.move(new Cell(4, 5));
    assertTrue(reversi.move(new Cell(5, 4)));
    assertTrue(reversi.getState().getWinner().isPresent());
    assertSame(reversi.getState().getWinner().get(), Player.BLACK);
    assertSame(reversi.getState().getCurrentPhase(), Phase.FINISHED);
    assertFalse(reversi.move(new Cell(5, 4)));
  }

  @Test
  void finishedWhiteGameTest() {
    reversi.getState().setCurrentPlayer(Player.WHITE);
    reversi.move(new Cell(3, 3));
    reversi.move(new Cell(3, 4));
    reversi.move(new Cell(4, 4));
    reversi.move(new Cell(4, 3));

    reversi.move(new Cell(3, 5));
    reversi.move(new Cell(2, 5));
    reversi.move(new Cell(1, 5));
    reversi.move(new Cell(3, 6));
    reversi.move(new Cell(4, 7));
    reversi.move(new Cell(3, 2));
    reversi.move(new Cell(3, 1));
    reversi.move(new Cell(4, 5));
    assertTrue(reversi.move(new Cell(5, 4)));
    assertTrue(reversi.getState().getWinner().isPresent());
    assertSame(reversi.getState().getWinner().get(), Player.WHITE);
    assertSame(reversi.getState().getCurrentPhase(), Phase.FINISHED);
  }

  @Test
  void drawGameTest() {
    reversi.move(new Cell(3, 3));
    reversi.move(new Cell(3, 4));
    reversi.move(new Cell(4, 4));
    reversi.move(new Cell(4, 3));

    reversi.move(new Cell(4, 2));
    reversi.move(new Cell(3, 2));
    reversi.move(new Cell(2, 1));
    reversi.move(new Cell(5, 1));

    reversi.move(new Cell(2, 5));
    reversi.move(new Cell(5, 5));
    reversi.move(new Cell(5, 4));
    reversi.move(new Cell(3, 5));

    reversi.move(new Cell(6, 6));
    reversi.move(new Cell(1, 6));
    reversi.move(new Cell(0, 7));
    reversi.move(new Cell(7, 7));

    reversi.move(new Cell(5, 6));
    reversi.move(new Cell(4, 6));
    reversi.move(new Cell(5, 7));
    reversi.move(new Cell(1, 5));

    reversi.move(new Cell(7, 6));
    reversi.move(new Cell(1, 7));
    reversi.move(new Cell(3, 6));
    reversi.move(new Cell(3, 7));

    reversi.move(new Cell(2, 4));
    reversi.move(new Cell(3, 1));
    reversi.move(new Cell(0, 5));
    reversi.move(new Cell(1, 1));

    reversi.move(new Cell(3, 0));
    reversi.move(new Cell(1, 4));
    reversi.move(new Cell(0, 1));
    reversi.move(new Cell(2, 0));

    reversi.move(new Cell(2, 6));
    reversi.move(new Cell(2, 2));
    reversi.move(new Cell(6, 0));
    reversi.move(new Cell(0, 2));

    reversi.move(new Cell(4, 1));
    reversi.move(new Cell(1, 0));
    reversi.move(new Cell(1, 2));
    reversi.move(new Cell(5, 3));

    reversi.move(new Cell(5, 2));
    reversi.move(new Cell(6, 1));
    reversi.move(new Cell(6, 2));
    reversi.move(new Cell(7, 1));

    reversi.move(new Cell(6, 3));
    reversi.move(new Cell(7, 3));
    reversi.move(new Cell(5, 0));
    reversi.move(new Cell(2, 7));

    reversi.move(new Cell(2, 3));
    reversi.move(new Cell(1, 3));
    reversi.move(new Cell(0, 4));
    reversi.move(new Cell(6, 5));

    reversi.move(new Cell(6, 4));
    reversi.move(new Cell(4, 5));
    reversi.move(new Cell(7, 5));
    reversi.move(new Cell(6, 7));

    reversi.move(new Cell(4, 0));
    reversi.move(new Cell(7, 2));
    reversi.move(new Cell(7, 4));
    reversi.move(new Cell(4, 7));

    reversi.move(new Cell(7, 0));
    reversi.move(new Cell(0, 3));
    reversi.move(new Cell(0, 0));
    reversi.move(new Cell(0, 6));

    assertTrue(reversi.getState().getAllCellsOfPlayer(Player.WHITE).size() == 32);
    assertTrue(reversi.getState().getAllCellsOfPlayer(Player.BLACK).size() == 32);
    assertTrue(reversi.getState().getCurrentPhase() == Phase.FINISHED);
    assertFalse(reversi.move(new Cell(3, 3)));
  }
}
