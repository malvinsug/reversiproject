package de.lmu.ifi.sosylab.reversi.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class GameStateTest {

  @BeforeEach
  void setUp() {}

  @AfterAll
  void tearDown() {}

  @Test
  void testCopyState() {
    GameState state = new GameState();
    simpleCopyVerification(state);
  }

  void simpleCopyVerification(GameState state) {
    GameState realState = new GameState();
    GameState copyState = state.makeCopy();

    assertNotEquals(realState, copyState);
    assertEquals(realState.getCurrentPhase(), copyState.getCurrentPhase());
    assertEquals(realState.getCurrentPlayer(), copyState.getCurrentPlayer());
    assertEquals(realState.getMoveCounter(), copyState.getMoveCounter());
  }

  @Test
  void setAndGetPhaseTest() {
    GameState state = new GameState();

    Phase expectedPhase = Phase.FINISHED;
    state.setCurrentPhase(expectedPhase);
    assertEquals(expectedPhase, state.getCurrentPhase());

    expectedPhase = Phase.WAITING;
    state.setCurrentPhase(expectedPhase);
    assertEquals(expectedPhase, state.getCurrentPhase());

    expectedPhase = Phase.RUNNING;
    state.setCurrentPhase(expectedPhase);
    assertEquals(expectedPhase, state.getCurrentPhase());
  }

  @Test
  void setAndGetPlayerTest() {
    GameState state = new GameState();

    Player expectedPlayer = Player.BLACK;
    state.setCurrentPlayer(expectedPlayer);
    assertEquals(expectedPlayer, state.getCurrentPlayer());

    expectedPlayer = Player.WHITE;
    state.setCurrentPlayer(expectedPlayer);
    assertEquals(expectedPlayer, state.getCurrentPlayer());
  }

  @Test
  void testWinnerBlack() {
    GameState state = new GameState();
    state.setWinner(Optional.of(Player.BLACK));
    state.setCurrentPhase(Phase.FINISHED);
    assertEquals(Optional.of(Player.BLACK), state.getWinner());
    state.setCurrentPhase(Phase.RUNNING);
    state.setCurrentPhase(Phase.RUNNING);
    assertThrows(IllegalStateException.class, () -> state.getWinner());
  }

  @Test
  void testWinnerWhite() {
    GameState state = new GameState();
    state.setWinner(Optional.of(Player.WHITE));
    state.setCurrentPhase(Phase.FINISHED);
    assertEquals(Optional.of(Player.WHITE), state.getWinner());
    state.setCurrentPhase(Phase.RUNNING);
    assertThrows(IllegalStateException.class, () -> state.getWinner());
  }

  @Test
  void testIncreaseMoveCounter() {
    GameState state = new GameState();
    for (int i = 0; i < 20; i++) {
      assertEquals(i, state.getMoveCounter());
      state.increaseMoveCounter();
    }
  }

  @Test
  void testToString() {
    GameState state = new GameState();
    assertNotEquals(state.toString(), null);
    assertEquals(state.toString().getClass(), String.class);
  }
}
