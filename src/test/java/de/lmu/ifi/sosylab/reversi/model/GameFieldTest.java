package de.lmu.ifi.sosylab.reversi.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

@TestInstance(Lifecycle.PER_CLASS)
public class GameFieldTest {

  @BeforeAll
  void setUp() {}

  GameField globalField = new GameField();

  @AfterAll
  void tearDown() {}

  @Test
  void testTheField() {
    Player player = Player.WHITE;
    testField(player);
    player = Player.BLACK;
    testField(player);
  }

  @ParameterizedTest
  @CsvSource(value = {"9,9", "-1,3", "-2,9", "9,1", "12,-1"})
  void testRemove(int col, int row) {
    Cell cell = new Cell(col, row);
    GameField field = new GameField();
    assertThrows(IllegalArgumentException.class, () -> field.remove(cell));
    assertThrows(IllegalArgumentException.class, () -> field.set(cell, new Disk(Player.BLACK)));
    assertThrows(IllegalArgumentException.class, () -> field.get(cell));
  }

  private void testField(Player player) {
    int size = GameField.SIZE;
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        GameField field = new GameField();
        Cell cell = new Cell(i, j);
        Disk disk = new Disk(player);
        GameField copyField = new GameField(field);
        testSetAndGet(field, cell, disk);
        testSetAndGet(copyField, cell, disk);

        isCellOfPlayer(field, cell, player, true);
        isCellOfPlayer(field, cell, Player.getOpponentOf(player), false);
        isCellOfPlayer(copyField, cell, player, true);
        isCellOfPlayer(copyField, cell, Player.getOpponentOf(player), false);

        Disk removedDisk = field.remove(cell);
        copyField = new GameField(field);
        assertEquals(disk, removedDisk);
        isCellOfPlayer(field, cell, player, false);
        isCellOfPlayer(field, cell, Player.getOpponentOf(player), false);
        isCellOfPlayer(copyField, cell, player, false);
        isCellOfPlayer(copyField, cell, Player.getOpponentOf(player), false);
      }
    }
  }

  void isCellOfPlayer(GameField field, Cell cell, Player player, Boolean expected) {
    Boolean actualValue = field.isCellOfPlayer(player, cell);
    assertEquals(expected, actualValue);
  }

  void testSetAndGet(GameField gameField, Cell cell, Disk disk) {
    gameField.set(cell, disk);
    Player actualDiskPlayer = gameField.get(cell).get().getPlayer();
    Player expectedDiskPlayer = disk.getPlayer();
    assertEquals(expectedDiskPlayer, actualDiskPlayer);
  }

  @Test
  void testSize() {
    assertEquals(GameField.SIZE, 8);
  }

  @Test
  void outOfBoundTest() {

    for (int i = 0; i < 10; i = i + 1) {
      Cell cell = new Cell(-1, i);
      for (Player player : Player.values()) {
        outOfBoundTest(cell, player);
      }
    }
    for (int i = 0; i < 13; i = i + 1) {
      Cell cell = new Cell(i, 10);
      for (Player player : Player.values()) {
        outOfBoundTest(cell, player);
      }
    }
    for (int i = 1; i < 13; i = i + 1) {
      Cell cell = new Cell(-i, -i);
      for (Player player : Player.values()) {
        outOfBoundTest(cell, player);
      }
    }
    for (int i = 9; i < 17; i = i + 1) {
      Cell cell = new Cell(i, i);
      for (Player player : Player.values()) {
        outOfBoundTest(cell, player);
      }
    }
    for (int i = 0; i < GameField.SIZE; i = i + 1) {
      Cell cell = new Cell(i, -1 - i);
      for (Player player : Player.values()) {
        outOfBoundTest(cell, player);
      }
    }
  }

  void outOfBoundTest(Cell cell, Player player) {
    assertThrows(IllegalArgumentException.class, () -> outOfBound(cell, new Disk(player)));
  }

  void outOfBound(Cell cell, Disk disk) {
    globalField.set(cell, disk);
  }

  @Test
  void sizeTest() {
    // if the size changes some features may break and need to be adjusted
    assertEquals(GameField.SIZE, 8);
  }
}
