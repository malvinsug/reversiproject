package de.lmu.ifi.sosylab.reversi.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class ReversiUtilityTest {

  @BeforeAll
  void setUp() {}

  @AfterAll
  void tearDown() {}

  @Test
  void testPlayer() {
    String string = "A1,B1";
    List<Cell> targets = new ArrayList<>();
    targets.add(new Cell(0, 0));
    targets.add(new Cell(1, 0));
    List<Cell> actualList = ReversiUtility.stringOfMovesToGameMoves(string);
    assertEquals(targets, actualList);
  }

  @Test
  void testCreateGameState() {
    List<Cell> black = new ArrayList<>();
    black.add(new Cell(0, 0));
    black.add(new Cell(0, 1));
    List<Cell> white = new ArrayList<>();
    white.add(new Cell(6, 6));
    white.add(new Cell(4, 4));
    GameState state = ReversiUtility.createGameState(black, white);
    Set<Cell> atualCells = state.getAllCellsOfPlayer(Player.BLACK);
    assertTrue(atualCells.containsAll(black));
    atualCells = state.getAllCellsOfPlayer(Player.WHITE);
    assertTrue(atualCells.containsAll(white));
  }

  @Test
  void testCreateGameState2() {
    List<Cell> black = new ArrayList<>();
    black.add(new Cell(0, 0));
    black.add(new Cell(0, 1));
    List<Cell> white = new ArrayList<>();
    white.add(new Cell(6, 6));
    white.add(new Cell(4, 4));
    int moveCounter = 6;
    Player currentPlayer = Player.BLACK;
    Phase currentPhase = Phase.RUNNING;
    GameState state =
        ReversiUtility.createGameState(
            black, white, currentPlayer, moveCounter, currentPhase, Optional.empty());
    Set<Cell> atualCells = state.getAllCellsOfPlayer(Player.BLACK);
    assertTrue(atualCells.containsAll(black));
    atualCells = state.getAllCellsOfPlayer(Player.WHITE);
    assertTrue(atualCells.containsAll(white));
    assertEquals(currentPhase, state.getCurrentPhase());
    assertEquals(currentPlayer, state.getCurrentPlayer());
    assertEquals(moveCounter, state.getMoveCounter());
  }
}
