package de.lmu.ifi.sosylab.reversi.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class PlayerTest {

  @BeforeAll
  void setUp() {}

  @AfterAll
  void tearDown() {}

  @Test
  void testPlayer() {
    extractedTest(Player.BLACK);
    extractedTest(Player.WHITE);
  }

  private void extractedTest(Player thisPlayer) {
    Player player = thisPlayer;
    Player otherPlayer = Player.getOpponentOf(thisPlayer);
    assertNotEquals(player, otherPlayer);
  }

  @Test
  void testOpponent() {

    assertEquals(Player.WHITE, Player.getOpponentOf(Player.BLACK));
    assertEquals(Player.BLACK, Player.getOpponentOf(Player.WHITE));
  }

  @Test
  void testToString() {
    assertNotEquals(Player.BLACK.toString(), Player.WHITE.toString());
    assertNotEquals(Player.BLACK.toString(), null);
    assertNotEquals(Player.WHITE.toString(), null);
  }
}
