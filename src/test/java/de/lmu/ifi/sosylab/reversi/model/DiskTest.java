package de.lmu.ifi.sosylab.reversi.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class DiskTest {

  @BeforeAll
  void setUp() {}

  @AfterAll
  void tearDown() {}

  @Test
  void whiteDisk() {
    testPlayer(Player.WHITE);
    testPlayer(Player.getOpponentOf(Player.BLACK));
  }

  @Test
  void blackDisk() {
    testPlayer(Player.BLACK);
    testPlayer(Player.getOpponentOf(Player.WHITE));
  }

  void testPlayer(Player player) {
    Disk disk = new Disk(player);

    Player actualPlayer = disk.getPlayer();
    Player expectedPlayer = player;

    assertEquals(expectedPlayer, actualPlayer);

    disk.flipPlayer();
    Player actualPlayer2 = disk.getPlayer();

    assertNotEquals(expectedPlayer, actualPlayer2);
  }
}
