# ReadMe

We sadly had at the end some time problems since 2 groub members got sick the last week.

## Build
Most builds are here zipped and the jar can be found at lib folder.

If the size of the jar allowes it, we included also all test reports created
by the plugins like pitest.

For the network game you need to start the server from the seperate jar.

If it is not clear how to run the jar read the global readme.

We only recently changed build folder from build folder to this one so there could be some confusion in older files.

## Test Reports
For the last jar we added the test reports in a seperat zip in the folder.
You can also run all test reports from the gradle setup e.g. `gradle pitest`