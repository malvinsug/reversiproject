# Reversi project

This project consists of a simple gradle config and example implementation of the game Reversi done for the SEP WS19/20 at LMU Munich.
The main goal of this game is to place disks on a board in a strategic way to have in the end more disks than the other player.
Depending on what spot a move was made the disks, which were already on the board, will flip.
For more details on the game please visit the [wikipedia](https://de.wikipedia.org/wiki/Othello_(Spiel)) for this game. 
(some rules for this game will differ from the official ones)

For more information on the project visit the [wiki](https://gitlab2.cip.ifi.lmu.de/sosy-lab/sep-ws-19/team-tim/-/wikis/home) of this project.

## Dependencies

The project requires Java 11, jackson libery und google guava 

The project is built with `gradle`, version 5.6.4. The provided `gradlew` wrapper automatically downloads and uses
the correct gradle version.


## Building the Project

On Linux and Mac OS, run the following command from the project's root directory to compile the program,
run all checks and create an executable jar:

```
./gradlew build jar
```

On Windows, run the following command from the project's root directory to compile the program,
run all checks and create an executable jar:

```
./gradlew.bat build jar
```

If your path for gradle are set, you can also call `gradlew someCommand`in your project folder. 

For vscode there is also a predefined tasks file for easy execution of tasks.

If the command succeeds, the jar is found in `build/libs/sample.jar`.
This jar can be executed with `java -jar build/libs/sample.jar`


## Running the Program

To run the program during development without any checks, run `./gradlew run` .

#### Running the jar

executed in the same folder as the jar (can be zipped) in the [build](https://gitlab2.cip.ifi.lmu.de/sosy-lab/sep-ws-19/team-tim/tree/master/build) or releases folder:
`java -jar all-jar.jar`

##### For seperate dependencies:
 open the zip with buildX_inc_dependencies (X is a number e.g 3) in the build folder

for Windows
`\sample\sample\bin\sample.bat`

for linux:
`\sample\sample\bin\sample`


if you specify the dependencies you can run
`java <Path> -jar sample.jar`

###### Help for jar

* if you are not in the same folder as the file please, switch to it
* if you have not set the path to java pleased do it or provide the path to java before executing
* if it still does not work, please check your java version to see if it is 11 or higher
* check if all path to the dependencies are correct

The jar is found in the build folder and is zipped. It is needs to be unzipped first before it is able to running.
The different build versions are in ascending order(e.g. the highest number is the newest build)



## Running the plugins

to generate test report:

```
gradlew test jacocoTestReport
```

For more information see: <https://www.jacoco.org/jacoco/>

to automatically format:
```
gradlew spotlessApply
```

For more information: <https://github.com/diffplug/spotless/tree/master/plugin-gradle>

to run local your own sonarqube

```
gradlew sonarqube
For full setup:
gradlew sonarqube -Dsonar.projectKey=reversi -Dsonar.host.url=http://localhost:9000 -Dsonar.login=token
```

You need to install it separately on your own PC

See:
<https://www.sonarqube.org>


pitest:

to run a mutagen use the command

```
gradlew pitest
```

For more information see: <https://pitest.org,> <https://gradle-pitest-plugin.solidsoft.info>
