# Testing


For testing we mainly used pitest to see the line coverage and mutation coverage.
We tried to cover what we though is most useful for your game and what is good to
test automatical.

## Game
For the game it self we tested quite good and made sure to test what should be a valid situation.
(less focused on abnormal situations)

## AIGame
We decided to not test this part. It is quite hard to test it since first it is not based so much on some counter-source we could cross test. The most important thing about the AI should be that it works fast, which it does. Does valid moves and does not crash. This part we tested per hand and got no problems with it.

## Network Game

It also seems to use better to test it per hand.

## View

It also seems to use better to test it per hand.


# Test Reports

The test reports are all included in the zip files for all the different builds.(as long as the size was small enough)
If you want to create them from the source code, use `gradle pitest`