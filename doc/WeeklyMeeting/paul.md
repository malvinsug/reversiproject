# Status log for Paul

## 21.01.20

#### Achievements of the last week
* hold a list of gamethreads for multiple games at once
* list of random game
* list of matching games
* client/server interface updates
* further stabilization
* handling views  correctly
* choose black/white + match/random + join/start

#### The plan for next week
* test connection from seperate pc 
* error handling
* further stabilization

#### Challenges and difficulties
* Wrong concept of gamethreads, had to refactor for client to start as black or white
* First state not updated correctly


## 14.01.20

#### Achievements of the last week
* implement gamethread for server
* testing with 2 clients on one pc
* further server and client fittings
* handling communication of 2 clients


#### The plan for next week
* hold a list of gamethreads for multiple games at once
* further stabilization
* handling views  correctly
* test connection from seperate pc 

#### Challenges and difficulties
* Locking and Unlocking the views of the clients appears to be more difficult than expected

## 07.01.20

#### Achievements of the last week
* concept of the game integration into the server
* setup simple echo server
* setup echo client
* setup pdu and pass gamestate
* integrate client and server to ReversiMain (not finished + tested)

#### The plan for next week
* finish integration of client and server in game logic
* start testing with 2 clients on one computer
* test connection from seperate pc 

#### Challenges and difficulties
* GameView stops working when server thread is started

## 17.12.19

#### Achievements of the last week
* read into java sockets
* review DONE
* review Requirements
* review Guidelines
* access gitlab
* understand reversi game
* understand project code
* understand gitlab
* experimenting for plugin evaluation

#### The plan for next week
* read further into java sockets
* start with implementation of ai for reversi

#### Challenges and difficulties
* get to know how to use vcs (when to create branch?, how often commit?)

