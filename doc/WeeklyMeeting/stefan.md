# Status log for Stefan

## 20.01.20
#### Achievements of the last week
* reviewed code of heuristic
* refactored code and rewrote partially the heuristic
* no bugs in game logic or ai founds last week-> no bug fixing
* implemented static factory for creating new cells for performance boosting
* created issues on gitlab
* evaluted if it is still possible to include a deep learning network based on alphaZero(sadly it would take to much time)

#### The plan for next week
* presentation preparation
* helping to finish features
* creating final jar

#### Challenges and difficulties


## 14.01.20
#### Achievements of the last week
* Gradle build fat jar
* implemented logger for easier bug fixes
* fixed bugs
* fine tuning of AI
* updated wiki
* checked progress

#### The plan for next week
* update gitlab wiki and docs
* bug fixing

#### Challenges and difficulties
* settings of the logger


## 07.01.20
#### Achievements of the last week
* Gradle included pitest
* improved old test to have higher coverage and score in pitest
* implemented an minimax with alpha beta pruding (not fully finished)
* AI game
* partially improved gitlab docs and wiki

#### The plan for next week
* update gitlab wiki and docs
* workout progress of the project
* further work on the ai and tests
* jumper for urgened programming tasks
* bug fixing

#### Challenges and difficulties
* a few problems with gradle
* testing and how to best do it
* finding what caused the bug

## 17.12.19

#### Achievements of the last week
* Gradle Setup
* Some Basic Tests
* Reading on AI
* setting up Gitlab wiki and project guide lines
* Documentation of project
* write google docs for features
* write an utility class for later use

#### The plan for next week
* update gitlab wiki and docs
* need to be discussed in group what task is needed the most
* avoid using google docs

#### Challenges and difficulties
* not sure how everything works in gitlab
* config of gradle
* finding out how i can get vscode to do everything for me
