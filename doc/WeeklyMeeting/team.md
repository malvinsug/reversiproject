# Status log for the team-tim

## 20.01.20

#### Achievements of the last week
* refactored heuristic
* static cell factory
* improved view-network interaction
* improved network (stabilization, threads, ...)

#### The plan for next week
* finishing up the project
* creating final jar
* presentation


#### Challenges and difficulties

## 14.01.20

#### Achievements of the last week
* logger
* bug fixes
* gui updated
* ai finished
* hot seat finished
* network funktional improvements

#### The plan for next week
* network
* more tests and bug fixes
* view modifications

#### Challenges and difficulties

## 07.01.20

#### Achievements of the last week
* working gui (e.g you can play a game on it)
* improved tests for the base code
* pitest plugin for improving tests
* network basics
* ai game implemented
* game logic finished

#### The plan for next week
* network
* more tests and bug fixes
* connect the different parts (game-view-network-ai)
* view modifications
* evaluate current project status and prioritze

#### Challenges and difficulties
* fixing bugs

## 17.12.19

#### Achievements of the last week
* write and review DONE
* write down Requirments and review them
* write down project guidelines and review them
* write down meeting points
* learn junit testing
* setup IDEs and other access to gitlab
* getting to know gitlab
* study reversi game rules
* setup gradle including useful plugins like spotless, sonarqube, jacoco
* google docs for design of different features
* todo list for meeting
* test gradle plugins (only tested on a few workspaces)
* write down the weekly task file
* trying to understand code of week 1
* trying to understand the client-server architecture
* unit tests for the some project code


#### The plan for next week
* getting a working gui with limited functionallity
* getting the move function to work
* workout how to implement the AI
* workout how to implement the netgame
* no more google docs :(
* getting used to gradle plugins
* get a better understanding of git and gitlab


#### Challenges and difficulties
* problems with gitlab
* first week problems

## 11.12.19

### Already done tasks
* thinking about what can be reused from the chess project
* setting up a early prototype based on the code of the chess project
* edit gitignore
* work out what features need heavy rewriting (Game logic especial move function, AI, netgame and gui)
* split the team into features
* helping eachother with gitlab and ide setup
* worked out what features require further reading to fully understand how to implement. (testing, network and ai)
