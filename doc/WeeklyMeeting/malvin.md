# Status log for Malvin

## 21.01.2020

#### Achievements of the last week
* created a security query for received pdu
* review the code from Paul
* review a reviewed code from Stefan and make some improvement
* coverting json to java object


#### The plan for next week
* deserialize json to java object.
* help improving view or tests if needed

#### Challenges and difficulties
* deserializing json

# Status log for Malvin

## 14.01.2020

#### Achievements of the last week
* fixed the bug from last meeting
* do pair programming with Leo for testing the game logic
* adding new heuristics to the AI.
* little fix in view game when the game ends.

#### The plan for next week
* making tests for AIGame (?)
* help paul making the network game (?)

#### Challenges and difficulties
* once the game is finished, the message windows pops up 2 times.
* should I rename static variables in my heuristic?

# Status log for Malvin

## 06.01.2020

#### Achievements of the last week
* finished the game logic
* add the basic functionality to the GUI
* reading a scoring method for reversi game

#### The plan for next week
* fix the bugs

#### Challenges and difficulties
* Bugs towards the end of the game.
* It would be better if there's a pause between the players in AI game.

# Status log for Malvin

## 16.12.2019

#### Achievements of the last week
* prototyping getIsInLine function
* replacing getConnectedCells function with isConnectedCell
* fixing getPossibleMovesForDisk
* prototyping 'unit test' -> Playground class
* installing sonarqube
* reading definition of done
* reading 2 given links from Stefan about unit testing
* running a program with spotless coverage

#### The plan for next week
* finishing move function
* saving moves history
* prototyping unit test for move function
* maybe do a pair programming with leo ?

#### Challenges and difficulties
* redundancy in getIsInLine
* I haven't found a way to use getIsInLine function as flipping relevant disks at the same time
* I am still not sure when to commit and when to push
* git chekout problem
