# Status log for Ege

## 21.01.2020

#### Achievements of the last week
* designed a settings menu(funtionality incomplete)
* laid foundations for previous&next move buttons, network lobby menu 

#### The plan for next week
* finish the remaining GUI work and also the related network stuff as fast as possible

#### Challenges and difficulties
* figuring out a way to have settings affect game creations
* figuring out how to save and reach every state of a match

## 14.01.2020

#### Achievements of the last week
* implementing a setting menu(not quite finished)
* started implementing a previous moves mini-feature
* started implementing a network lobby menu
* read quite a lot of the commits by other members(for no particular reason, it just happened)

#### The plan for next week
* finishing the implementations that I started
* extra stuff if nothing goes bad

#### Challenges and difficulties
* nothing particularly problematic just the usual design indecisiveness

## 07.01.2020

#### Achievements of the last week
* implemented basic working GUI with functionality
* checked & read both AI and Server related codings done by other members
* got used to Gitlab and related things

#### The plan for next week
* help fixing bugs (current&future)
* further GUI depending on what we want to add onto it
* helping AI and/or Server side of the game

#### Challenges and difficulties
* learning more about java for alternative/better game design

## 17.12.2019

#### Achievements of the last week
* reviewed DoD
* reviewed Guidelines
* read project requirement
* studied Reversi rules & workings
* studied current code
* getting used to working with Gitlab
* read JUnit Testing

#### The plan for next week
* completing View adaptation to Reversi game
* getting more tasks for completion of the first feature release
* getting used to Gitlab
* reading & studying more about the possible AI implementations to keep up

#### Challenges and difficulties
* getting used to Gitlab(when to create branches etc.)
* didn't really get the scope of what I had to do(specific to this week)
