# Status log for Leopold

## 21.01.20

## Achievements of the last week
* read some code
* was slacking with the tests last week

#### The plan for next week
* write finishing tests
* test server + view

#### Challenges and difficulties


## 14.01.20

## Achievements of the last week
* created new tests with Malvin
* -> Reversi is almost fully tested
* tested the game manually

#### The plan for next week
* write more tests
* maybe for AI / Heuristic?

#### Challenges and difficulties
* couldn't quite figure out a gamestate to test a draw
* maybe redundant tests

## 07.01.20

## Achievements of the last week
* tried some different gamesatetes/ game-endings
* stayed up to date with changes from others
* tested the current game a bit

#### The plan for next week
* get more used to JUnit testing
* commiting JUnit tests

#### Challenges and difficulties
* is an automated run down to a specific gamestate/ ending worth for a JUnit test?


## 20.12.19

### Merge Test

## `16.12.19`

#### Achievements of the last week
* `read into code`
* `started reading first "feature"`
* `got used to Git`
* `read the Reversi rules`

#### The plan for next week
* `review all of Malvins code`
* `maybe review other code?`
* `get to know JUnit testing more`
* `get more tasks`

#### Challenges and difficulties
* `have to get used to reviewing code from others`
* `(time consuming at first)`
