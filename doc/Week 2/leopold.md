# Status log for Leopold

## `16.12.19`

#### Achievements of the last week
* `read into code`
* `started reading first "feature"`
* `got used to Git`
* `read the Reversi rules`

#### The plan for next week
* `review all of Malvins code`
* `maybe review other code?`
* `get to know JUnit testing more`
* `get more tasks`

#### Challenges and difficulties
* `have to get used to reviewing code from others`
* `(time consuming at first)`
