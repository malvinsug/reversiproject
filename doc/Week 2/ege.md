# Status log for Ege

## `<17.12.2019>`

#### Achievements of the last week
* `<reviewed DoD>`
* `<reviewed Guidelines>`
* `<read project requirement>`
* `<studied Reversi rules & workings>`
* `<studied current code>`
* `<getting used to working with Gitlab>`
* `<read JUnit Testing>`

#### The plan for next week
* `<completing View adaptation to Reversi game>`
* `<getting more tasks for completion of the first feature release>`
* `<getting used to Gitlab>`
* `<reading & studying more about the possible AI implementations to keep up>`

#### Challenges and difficulties
* `<getting used to Gitlab(when to create branches etc.)>`
* `<didn't really get the scope of what I had to do(specific to this week)>`
