# Status log for Stefan

## `<17.12.19>`

#### Achievements of the last week
* `<Gradle Setup>`
* `<Some Basic Tests>`
* `<Reading on AI>`
* `<setting up Gitlab wiki and project guide lines>`
* `<Documentation of project>`

#### The plan for next week
* `<update gitlab wiki and docs>`
* `<need to be discussed in group what task is needed the most>`

#### Challenges and difficulties
* `<not sure how everything works in gitlab>`
* `<config of gradle>`
* `<finding out how i can get vscode to do everything for me>`
