# Status log for Paul

## `17.12.19`

#### Achievements of the last week
* `read into java sockets`
* `review DONE`
* `review Requirements`
* `review Guidelines`
* `access gitlab`
* `understand reversi game`
* `understand project code`
* `understand gitlab`
* `experimenting for plugin evaluation`

#### The plan for next week
* `read further into java sockets`
* `start with implementation of ai for reversi`

#### Challenges and difficulties
* `get to know how to use vcs (when to create branch?, how often commit?)`
