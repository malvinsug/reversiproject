# Status log for Malvin

## `<16.12.2019>`

#### Achievements of the last week
* `<prototyping getIsInLine function>`
* `<replacing getConnectedCells function with isConnectedCell>`
* `<fixing getPossibleMovesForDisk>`
* `<prototyping 'unit test' -> Playground class>`
* `<installing sonarqube>`
* `<reading definition of done>`
* `<reading 2 given links from Stefan about unit testing>`
* `<running a program with spotless coverage>`

#### The plan for next week
* `<finishing move function>`
* `<saving moves history>`
* `<prototyping unit test for move function>`
* `<maybe do a pair programming with leo ?>`

#### Challenges and difficulties
* `<redundancy in getIsInLine>`
* `<I haven't found a way to use getIsInLine function as flipping relevant disks at the same time>`
* `<I am still not sure when to commit and when to push>`
* `<git chekout problem>`
